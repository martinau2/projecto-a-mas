﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TutorialTarget : MonoBehaviour
{
    public UnityEvent OnHit;
    public Sprite hitSprite;

    public void OnHitTrigger() {
        GetComponent<SpriteRenderer>().sprite = hitSprite;
        OnHit.Invoke();

    }
}
