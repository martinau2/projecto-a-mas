﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class Boss : MonoBehaviour
{

    public SerializableIntEvent dmgEvent;
    public SerializableEvent startEncounter;
    public SerializableEvent finishEncounter;
    public float bossHealth;
    public bool dead;
    [Header("Player")]
    public Transform player;
    [Header("Parametros Txt Dmg")]
    public GameObject dmgText;
    public float yOffSet = 2;
    public float xOffSet = 3;
    public int[] times;
    public string[] rank;



    public virtual void Start()
    {
        
        
           
        startEncounter.Raise();


        #region Adds Player to variable
        if (FindObjectOfType(typeof(PlayerBehaviour)) == null)
        {
            Debug.LogError("Error Player not found");
        }
        else
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
        }
        #endregion
    }
    #region Recibe el daño y instancia el texto
    public void TakeDmg(int dmg)
    {
        Func<float, bool> CheckCrit = dmgfloat =>
        {
            //Checkear si crit o si el daño es mas fuerte
            if (dmg > 10)
                return true;
            else return false;
        };
        bossHealth -= dmg;
        dmgTxt(dmg,CheckCrit(dmg));
        
    }
    #endregion
    #region Instancia el TextMeshPro con el daño
    public virtual void dmgTxt(float dmg,bool crit) {
        GameObject text = Instantiate(dmgText, new Vector2(transform.position.x + UnityEngine.Random.Range(-xOffSet, xOffSet),
            transform.position.y + yOffSet),
            Quaternion.identity,
            null);
        var dmgTextMeshPro = text.GetComponent<TextMeshPro>();
        dmgTextMeshPro.text = crit ? (dmg.ToString() + "!") : dmg.ToString();
        dmgTextMeshPro.color = crit ? Color.red : Color.white;
        

        
        
        Destroy(text, 1);

    }
    #endregion
    #region Death Behaviour
    public virtual void DeadBehaviour(Animator anim,string deathAnimString){
        finishEncounter.Raise();
        anim.SetTrigger(deathAnimString); ;
    }
    #endregion
    #region Rank Calculator
    //Calcula el rank otorgado en la pelea basado en el calculo echo
    public string RankLetters(int time)
    {
        List<string> Letters = new List<string>();
        for (int i = 0; i < times.Length - 1; i += 1)
        {
            if (time >= times[i] && time <= times[i + 1])
            {

                return rank[i];
                
               
               

            }

        }
        Debug.LogError("No Rank matches Kill Time");
        return ("Null");
    }

    #endregion
    public virtual void OnPlayerDeath() {
        player = null;
    }
}

    
