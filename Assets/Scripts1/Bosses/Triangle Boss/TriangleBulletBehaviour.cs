﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangleBulletBehaviour : StateMachineBehaviour
{
    public TriangleBehaviour behaviour;
    public float setTimer = 3;
    public bool endingAttck;
    float timer;
    public string deadPhaseString;
    public string timerDoneString;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        behaviour = animator.GetComponent<TriangleBehaviour>();
        timer = setTimer;
        behaviour.StartCoroutine(behaviour.PrepareBullets());
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (behaviour.player == null)
        {
            animator.SetTrigger(timerDoneString);
            Debug.Log("Player not Found");
            return;
        }
        if (behaviour.bossHealth <= 0)
        {
            behaviour.DeadBehaviour(animator, deadPhaseString);
        }
        if (timer <= 0)
        {
            
            animator.SetTrigger(timerDoneString);

        }
        else
        {
            timer -= Time.deltaTime;
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (endingAttck)
        {
            behaviour.StartCoroutine(behaviour.PrepareBullets());
        }
        animator.ResetTrigger(timerDoneString);
    }
    
}
