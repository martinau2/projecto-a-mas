﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinningTriangles : MonoBehaviour
{

    public SerializableIntEvent dmgToPlayer;
    public int dmg;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
        dmgToPlayer.Raise(dmg);
        }
    }
}
