﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangleBehaviour : Boss
{
    public static TriangleBehaviour instance { private set; get; }
    public SpriteRenderer bodySprite;
    public Material color;

    public int contactDamage;

    public bool rotating;
    public float rotationValue;
    public GameObject bullet;

    public GameObject[] spikes;
    public List<GameObject> spikeCopyes = new List<GameObject>();
    public GameObject body;
    public GameObject bigTriangles;
    public Transform startingPos;
    public Collider2D[] bossCollider;
    

    private void Awake()
    {
        #region Singleton
        if (instance != null)
        {
            Debug.Log("Error" + this);
            Destroy(this.gameObject);
            return;

        }
        instance = this;
        #endregion
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }
    private void Update()
    {
        //Color a rojo dependiendo de la vida.
        color.SetColor("_Color", new Color(bodySprite.color.r, bossHealth / 1000, bossHealth / 1000));
    }
    #region rota el jefe en direccion horaria
    public IEnumerator rotateMe(float duration,Transform rotated)
    {
        yield return new WaitForSeconds(0.3f);
        

        while (rotating)
        {
            float t = 0f;
            Quaternion target = rotated.rotation * Quaternion.AngleAxis(rotationValue, Vector3.forward);
            while (t < 1f)
            {
                t += Time.deltaTime / duration;
                rotated.rotation = Quaternion.Lerp(rotated.rotation, target, t*Time.deltaTime);
                yield return null;
            }
        }

        


    }
    #endregion
    #region sigue a el player
    public void FollowTarget(float speed,Vector3 target) {
        var dir = (target - transform.position).normalized;
        //transform.position = Vector3.Lerp(transform.position, target,speed*(Vector3.Distance(target,transform.position)/2*Time.deltaTime));
        if (Vector3.Distance(target, transform.position) == 7)
        {
            return;
        }
        else if (Vector3.Distance(target, transform.position) < 7)
        {
            transform.position += -dir * speed * Time.deltaTime;
        }
        else {
            transform.position += dir * speed*Time.deltaTime;
        }
    }
    #endregion
    #region prepara las balas en su posicion
    public IEnumerator PrepareBullets() {
        for (int i = 0; i < 5; i++)
        {
            var bullet_ = Instantiate(bullet, transform.position+new Vector3(-10+(i*5),transform.position.y,0),Quaternion.identity);
            bullet_.GetComponent<TriangleBullet>().collidersBoss = bossCollider;
            yield return new WaitForSeconds(0.5f);
        }

    }
    #endregion
    #region Prepara y activa los triangulos exteriores
    public void ReplaceSpikes()
    {
        foreach (var spike in spikes)
        {
            GameObject spike_ = Instantiate(spike,body.transform);
            spike.SetActive(false);
            spikeCopyes.Add(spike_);
        }

    }
    public void SpikesTarget() {
        foreach (var spike in spikeCopyes)
        {
            spike.transform.parent = null;
            StartCoroutine(TrackTarget(spike.transform, player,0.3f,8));
        }
    }
    public IEnumerator TrackTarget(Transform object_,Transform objective_,float proyspeed,float rotatespeed) {
        var proyspeed_ = (proyspeed + Random.Range(-proyspeed / 2, -proyspeed / 4));
        var rotatespeed_ = rotatespeed + Random.Range(-rotatespeed / 2, -rotatespeed / 4);
        while (true) {
            object_.rotateToTransform(rotatespeed_, objective_);
            yield return null;
            object_.Translate(-Vector3.up * proyspeed_);
            yield return null;
        }

    }
    #endregion

    public override void DeadBehaviour(Animator anim,string deadbehanim)
    {
        base.DeadBehaviour(anim,deadbehanim);
        dead = true;
    }

    

    public override void OnPlayerDeath()
    {
        base.OnPlayerDeath();
    }
}

public static class Helpers {
    public static void rotateToTransform(this Transform objectoToMove,float rotateSpeed, Transform objective)
    {
        Vector3 targetPoint = objective.position - objectoToMove.position;
        Quaternion targetRotation = Quaternion.LookRotation(objectoToMove.forward, -targetPoint);
        objectoToMove.rotation = Quaternion.Slerp(objectoToMove.rotation, targetRotation, Time.deltaTime * rotateSpeed);

    }

}
