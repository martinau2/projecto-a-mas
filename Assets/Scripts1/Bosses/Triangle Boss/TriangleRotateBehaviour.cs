﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangleRotateBehaviour : StateMachineBehaviour
{
    public TriangleBehaviour behaviour;
    public float setTimer = 3;
    float timer;
    public float rotationSpeed = 0.0001f;
    public float trackSpeed;
    public string deadPhaseString;
    public string timerDoneString;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        behaviour = animator.GetComponent<TriangleBehaviour>();
        behaviour.rotating = true;
        timer = setTimer;
        behaviour.StartCoroutine(behaviour.rotateMe(rotationSpeed, behaviour.bigTriangles.transform));
        Physics2D.IgnoreCollision(behaviour.bossCollider[0], behaviour.player.GetComponent<Collider2D>());
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (behaviour.player == null)
        {
            animator.SetTrigger(timerDoneString);
            Debug.Log("Player not Found");
            return;
        }
        if (behaviour.bossHealth <= 0)
        {
            behaviour.DeadBehaviour(animator,deadPhaseString);
        }
        behaviour.FollowTarget(trackSpeed, behaviour.player.position);

        if (timer <= 0)
        {
            animator.SetTrigger(timerDoneString);

        }
        else
        {
            timer -= Time.deltaTime;
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (behaviour.player != null)
        {
            Physics2D.IgnoreCollision(behaviour.bossCollider[0], behaviour.player.GetComponent<Collider2D>(), false);
        }
        behaviour.rotating = false;
        animator.ResetTrigger(timerDoneString);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
