﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraingleIdleBehaviour : StateMachineBehaviour
{
    public TriangleBehaviour behaviour;
    public int bossHealthTreshold;
    public float timer;
    public float minTime;
    public float maxTime;
    public List<string> statesTriggers;
    public string secondPhaseString;
    public string deadPhaseString;
    int nextPattern { get { return Random.Range(0, statesTriggers.Count); } }
    bool secondFase = false;
    
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        behaviour = animator.GetComponent<TriangleBehaviour>();
        timer = Random.Range(minTime, maxTime);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.transform.position = Vector3.Lerp(animator.transform.position, behaviour.startingPos.position ,3* Time.deltaTime);
        //behaviour.FollowTarget(Time.deltaTime * 4/timer, behaviour.startingPos.position);
        behaviour.bigTriangles.transform.rotation = Quaternion.Slerp(behaviour.bigTriangles.transform.rotation, Quaternion.Euler(new Vector3(0, 0, 0)), Time.deltaTime*4/timer);
        if (behaviour.player == null)
        {
            return;
        }
        HealthState(animator);
        if (timer <= 0) {
            int nextnumber = nextPattern;
            animator.SetTrigger(statesTriggers[nextnumber]);
            timer = Mathf.Infinity;
        }
        else
        {
            timer -= Time.deltaTime;
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        foreach (var trigger in statesTriggers) 
        {
            animator.ResetTrigger(trigger);
        }
    }

    private void HealthState(Animator animator)
    {
        if (!secondFase && behaviour.bossHealth <= bossHealthTreshold)
        {
            animator.SetTrigger(secondPhaseString);

        }
        else if (behaviour.bossHealth <= 0)
        {
            behaviour.DeadBehaviour(animator,deadPhaseString);
            animator.SetTrigger(deadPhaseString);
        }
    }
}
