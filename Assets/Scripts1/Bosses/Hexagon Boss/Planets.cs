﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planets : MonoBehaviour
{
    public float gravity = -100;
    // Start is called before the first frame update
    void Start()
    {
        Physics2D.gravity = Vector2.zero;
    }

    public void Attract(Transform body) {
        Vector3 gravityUP = (body.position - transform.position).normalized;
        Vector3 bodyUP = body.forward;

        body.GetComponent<Rigidbody2D>().AddForce(gravityUP * gravity*Mathf.Abs((1/Vector3.Distance(body.transform.position,transform.position))));
        
        body.right = Vector3.Cross(gravityUP, Vector3.forward);
    }


    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player") {
            var player = collision.transform;
            var rb = player.GetComponent<Rigidbody2D>();
            if (player)
            {
                rb.constraints = RigidbodyConstraints2D.FreezeRotation;
                Attract(player); 
            }
        }
    }
}
