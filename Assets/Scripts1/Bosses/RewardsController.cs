﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class RewardsController : MonoBehaviour
{
    public PlayerPowerUps powerUpsList;
    public PowerUps[] displayedPowerUps = new PowerUps[3];
    private Canvas canvas;
    public RewardDisplay reward_0, reward_1, reward_2;
    public List<PowerUps> powerUpsPool;
    PlayerBehaviour player;

    public SerializableEvent updatePowerUpsGUI;

    private void Start()
    {
        canvas = GetComponent<Canvas>();
    }
    public void ApplyPowerUp(PowerUps PowerUp_) {
        PowerUp_.AddPowerUp(powerUpsList);
        updatePowerUpsGUI.Raise();
    }
    public IEnumerator DisplayPowerUps() {
        player = GameState_Manager.instance.playerobj.GetComponent<PlayerBehaviour>();
        player.enabled = false;
        player.StopMoving();
        for (int i = 0; i < displayedPowerUps.Length; i++)
        {
            displayedPowerUps[i] = powerUpsPool[Random.Range(0, powerUpsPool.Count)];
        }

        canvas.enabled = true;
        reward_0.button.onClick.AddListener(delegate { ApplyPowerUp(displayedPowerUps[0]); });
        reward_1.button.onClick.AddListener(delegate { ApplyPowerUp(displayedPowerUps[1]); });
        reward_2.button.onClick.AddListener(delegate { ApplyPowerUp(displayedPowerUps[2]); });
        reward_0.name.text = displayedPowerUps[0].powerUpName;
        reward_1.name.text = displayedPowerUps[1].powerUpName;
        reward_2.name.text = displayedPowerUps[2].powerUpName;
        reward_0.sprite.sprite = displayedPowerUps[0].powerUpSprite;
        reward_1.sprite.sprite = displayedPowerUps[1].powerUpSprite;
        reward_2.sprite.sprite = displayedPowerUps[2].powerUpSprite;
        yield return null;
        player.StopMoving();
        reward_0.button.Select();
    }
    public void DisplayWrapper() {
        reward_0.button.enabled = true;
        reward_1.button.enabled = true;
        reward_2.button.enabled = true;
        StartCoroutine(DisplayPowerUps());
    }
    public void ChooseEnd() {
        reward_0.button.onClick.RemoveAllListeners();
        reward_1.button.onClick.RemoveAllListeners();
        reward_2.button.onClick.RemoveAllListeners();
        reward_0.button.enabled = false;
        reward_1.button.enabled = false;
        reward_2.button.enabled = false;
        player.enabled = true;
        canvas.enabled = false;
    }

}
[System.Serializable]
public class RewardDisplay {
    public Button button;
    public TextMeshProUGUI name;
    public Image sprite;

}
