﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleBulletState : StateMachineBehaviour
{
    CircleBehaviour behaviour;
    public float setTimer = 3;
    public float rotationSpeed = 1;
    float timer;
    float shootTimer;
    public float setShootTimer;
    
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        behaviour = animator.GetComponent<CircleBehaviour>();
        timer = setTimer;
        shootTimer = setShootTimer;
        animator.applyRootMotion = false;
        
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (behaviour.player == null)
        {
            animator.SetTrigger("timerDone");
            return;
        }
        if (behaviour.bossHealth <= 0)
        {
            behaviour.DeadBehaviour(animator,"death");
        }
        behaviour.rotateToPlayer(rotationSpeed);
        if (shootTimer <= 0)
        {
            behaviour.ShootBullet();
            shootTimer = setShootTimer;
        }
        else {
            shootTimer -= Time.deltaTime;
        }
        
        if (timer <= 0)
        {
            animator.SetTrigger("timerDone");

        }
        else {
            timer -= Time.deltaTime;
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
        animator.ResetTrigger("idle2");
        animator.ResetTrigger("timerDone");
        animator.applyRootMotion = true;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
