﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleIdleState : StateMachineBehaviour
{
    
    CircleBehaviour behaviour;
    public int bossHealthTreshold;
    public float timer;
    public float minTime;
    public float maxTime;
    public int nextPattern { get { return Random.Range(0, 2); } }
    bool secondFase = false;
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        behaviour = animator.GetComponent<CircleBehaviour>();
        timer = Random.Range(minTime, maxTime);
        

        behaviour.laserRay.enabled = false;
        //behaviour.laserParticle.Stop();

    }


    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.transform.rotation = Quaternion.Slerp(animator.transform.rotation, Quaternion.Euler(new Vector3(0, 0, 0)), Time.deltaTime);
        if (behaviour.player == null) {
            return;
        }
        HealthState(animator);
        if (timer <= 0)
        {
            int nextnumber = nextPattern;
            if (nextnumber == 0)
            {
                animator.SetTrigger("bulletAttck");

            }
            else if (nextnumber == 1)
            {
                animator.SetTrigger("rayAttck");
            }
            timer = Mathf.Infinity;
        }
        else
        {
            timer -= Time.deltaTime;
        }

    }

    private void HealthState(Animator animator)
    {
        if (!secondFase && behaviour.bossHealth <= bossHealthTreshold)
        {
            animator.SetTrigger("idle2");
        }
        else if (behaviour.bossHealth <= 0)
        {
            behaviour.DeadBehaviour(animator,"death");
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger("bulletAttck");
        animator.ResetTrigger("rayAttck");
    }

    
}
