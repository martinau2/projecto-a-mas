﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleRayState : StateMachineBehaviour
{
    CircleBehaviour behaviour;
    public float setTimer = 3;
    float timer;
    public float rotationSpeed = 4;
    public int damage;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer = setTimer;
        behaviour = animator.GetComponent<CircleBehaviour>();
        behaviour.laserHitParticle.SetActive(true);
        behaviour.laserRay.enabled = true;

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (behaviour.player == null)
        {
            animator.SetTrigger("timerDone");
            return;
        }
        behaviour.shootLaser(damage);
        if (behaviour.bossHealth <= 0)
        {
            behaviour.DeadBehaviour(animator,"death");
        }
        
        if (behaviour.rotating != true)
        {
            
            behaviour.StartCoroutine(behaviour.rotateMe(Time.deltaTime * rotationSpeed));

        }

        
        
        if (timer <= 0)
        {
            animator.SetTrigger("timerDone");

        }
        else
        {
            timer -= Time.deltaTime;
        }
    }
    

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //behaviour.laserParticle.Stop();
        behaviour.laserHitParticle.SetActive(false);
        behaviour.laserRay.enabled = false;
        
        animator.ResetTrigger("idle2");
        animator.ResetTrigger("timerDone");

        
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
