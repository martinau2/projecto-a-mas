﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;


public class CircleBehaviour : Boss
{
    
    public static CircleBehaviour instance { private set; get; }



    
    [Header("Bullet Param:")]
    public Transform bulletSpawner;
    public GameObject bullet;
    [Header("Laser Param:")]
    public ParticleSystem laserParticle;
    public GameObject laserHitParticle;
    public Transform laserInit;
    public LayerMask rayHit;
    public LineRenderer laserRay;
    [Space(25)]
    public bool rotating;
    [Range(0,-360)]public int rotationvalue = -20;
    IEnumerator spinBoss;
    SpriteRenderer sprite;
    
    private void Awake()
    
    {
        #region Singleton
        if (instance != null)
        {
            Debug.Log("Error" + this);
            Destroy(this.gameObject);
            return;

        }
        instance = this;
        #endregion
        
        
        sprite = GetComponent<SpriteRenderer>();
        
    }
    private void Update()
    {
        
        //Color a rojo dependiendo de la vida.
        sprite.material.SetColor("_Color",new Color(sprite.color.r, bossHealth / 1000, bossHealth / 1000));

    }
    




    //Rota el jefe en direccion al player.
    #region Rota hacia el jugador
    public void rotateToPlayer(float rotateSpeed) {
        Vector3 targetPoint = player.position - transform.position;
        Quaternion targetRotation = Quaternion.LookRotation(transform.forward, -targetPoint);
         transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotateSpeed);
        
    }
    #endregion
    //Activa el behaviour de muerte.
    #region Sobreescribe el metodo de muerte
    public override void DeadBehaviour(Animator anim,string stringDeadAnim)
    {
        base.DeadBehaviour(anim,"death");
        dead = true;
    }
    #endregion
    //Rota el jefe cada x grados por y tiempo.
    #region rota el jefe en direccion horaria
    public IEnumerator rotateMe(float duration)
    {
        rotating = true;
        yield return new WaitForSeconds(0.3f);


        for (int i = 0; i < 360 / Mathf.Abs(rotationvalue); i++)
            {


                
                float t = 0f;
                Quaternion target = transform.rotation * Quaternion.AngleAxis(rotationvalue, Vector3.forward);
                while (t < 1f)
                {
                    
                    t += Time.deltaTime / duration;
                    transform.rotation = Quaternion.Lerp(transform.rotation, target, t*Time.deltaTime);
                    yield return null;
                }
                
            
            }

        rotating = false;


    }
    #endregion
    //Dispara balas
    #region Logica de las balas
    public void ShootBullet() {
        GameObject bulletSpawn = Instantiate(bullet, null);
        bulletSpawn.transform.position = bulletSpawner.position;
        bulletSpawn.transform.rotation = bulletSpawner.rotation;

    }
    #endregion
    //Dispara el laser behaviour.
    #region Logica de el laser
    public void shootLaser(int Damage) {
        RaycastHit2D Laser = Physics2D.Raycast(laserInit.position, -laserInit.up,100,rayHit);
        
        
        if (Laser) {
            laserRay.SetPosition(0, new Vector3(laserInit.position.x, laserInit.position.y, 0));
            laserRay.SetPosition(1,Laser.point);
            laserHitParticle.transform.position = Laser.point;
            laserHitParticle.transform.position += new Vector3(0, 0, -0.5f);
            Debug.DrawLine(laserInit.position, Laser.point, Color.red, 0.1f);
            if (Laser.collider.tag == "Player")
            {
                
                dmgEvent.Raise(Damage);
                
            }
            
            
        }
        else
        {
            laserRay.SetPosition(0, new Vector3(laserInit.position.x, laserInit.position.y, 0));
            laserRay.SetPosition(1, new Vector3(laserInit.position.x, laserInit.position.y, 0) + -laserInit.up * 100);
            
            
        }
        
        
        
            
        
        
        
    }
    #endregion

}


