﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleBulletBehaviour : MonoBehaviour
{
    Rigidbody2D rb;
    public float speed;
    public LayerMask bounce;
    private float bounces;
    public float maxBounces;
    string names;
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {

        if (bounces >= maxBounces){
            Destroy(gameObject);
        }
        RaycastHit2D collide = Physics2D.Raycast(transform.position+transform.right, transform.position + transform.right, 1.7f, bounce);
        Debug.DrawLine(transform.position, transform.position+transform.right * 1.7f, Color.red);
        if (collide)
        {
            if (collide.collider.tag == "Ground")
            {
                Quaternion angle = Quaternion.FromToRotation(gameObject.transform.right, new Vector3(collide.normal.x, collide.normal.y, 0));
                transform.rotation = transform.rotation * angle;
                if (collide.collider.name == null || collide.collider.name != names) {
                    bounces += 1;

                }
                names = collide.collider.name;
                
            }
            else if (collide.collider.tag == "Player")
            {
                
                CircleBehaviour.instance.dmgEvent.Raise(2);
                Destroy(gameObject);
            }
        }
    }
    private void FixedUpdate()
    {
        rb.velocity = transform.right*speed;
        
    }
    
}
