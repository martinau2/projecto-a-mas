﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangleBullet : Bullet
{
    public LineRenderer objective;
    Vector3 objectiveDirection;
    bool shoot = false;
    public float hoverTimer = 3f;
    public Collider2D[] collidersBoss;
    public GameObject explosion;

    bool done = false;

    // Update is called once per frame
    protected override void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        var player = GameObject.FindGameObjectWithTag("Player");
        TargetObjective(player);
        foreach (var collider in collidersBoss)
        {
            Physics2D.IgnoreCollision(gameObject.GetComponent<Collider2D>(), collider);
        }

    }
    void Update()
    {
        if (shoot)
        {
            objective.enabled = false;
            transform.position = Vector3.MoveTowards(transform.position, objectiveDirection, 1*original_Param.speed);
        }
        if (transform.position == objectiveDirection && !done) {
            done = true;
            Destroy_(hoverTimer);
        }
    }

    private void TargetObjective(GameObject player)
    {
        objectiveDirection = player.transform.position;
        objective.SetPosition(0, transform.position);
        objective.SetPosition(1, objectiveDirection);
        Invoke("ActivateShoot", 1);
    }

    protected override void FixedUpdate()
    {


    }
    void ActivateShoot() {
        shoot = true;
    }
    private void Destroy_(float time)
    {
        Destroy(gameObject, time);
        Invoke("instantiateParticle", time-0.1f);
    }
    public void instantiateParticle() {
        Destroy(Instantiate(explosion, transform.position, Quaternion.identity, null), 2);
    }



    private void OnCollisionEnter2D(Collision2D collision)
    {

            

        if (((1 << collision.gameObject.layer) & original_Param.targetLayer) != 0)
        {
            dmgEvent.Raise(original_Param.dmg);

            shoot = false;
            Destroy_(0.05f);
        }
        else {
            shoot = false;
            Destroy_(0.05f);
        }

    }

}
