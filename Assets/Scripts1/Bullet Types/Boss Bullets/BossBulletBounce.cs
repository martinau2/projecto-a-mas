﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBulletBounce : Bullet
{
    public ParticleSystem particle;
    private float bounces;
    public float maxBounces;
    public int damageValue;

    public SoundPlayer sp;
    public Sound_FX impactSound;


    protected override void Start()
    {
        base.Start();
        sp.PlaySound(impactSound);
    }
    // Update is called once per frame
    void Update()
    {
        

    }
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        bounces += 1;
        Vector3 particlePos = new Vector3(collision.contacts[0].point.x, collision.contacts[0].point.y, 0);
        ParticleSystem Particles = Instantiate(particle, particlePos,Quaternion.identity, null);
        Particles.transform.position += new Vector3(0, 0, -0.5f);
        Destroy(Particles.gameObject,2f);
        if (bounces >= maxBounces)
        {

            sp.PlaySound(impactSound);
            Destroy(gameObject);
        }
        if (((1 << collision.gameObject.layer) & original_Param.targetLayer) != 0) {

            dmgEvent.Raise(damageValue + Mathf.RoundToInt(Random.Range(0,Mathf.Sin(damageValue * Random.Range(-2,2))*5)));
            sp.PlaySound(impactSound);
            Destroy(gameObject);
        }

    }

}
