﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Bullet : MonoBehaviour
{
    protected Rigidbody2D rb;
    public BulletType original_Param;
    public SerializableIntEvent dmgEvent;
    public PlayerPowerUps playerPowerUps;
    

    private void Awake()
    {
        original_Param.restore();

        if (playerPowerUps != null)
        {
            foreach (var Powers in playerPowerUps.bulletpowersUP)
            {
                if (Powers != null)
                {
                    Powers.ApplyPowerUp(original_Param);
                }
                else {
                    playerPowerUps.bulletpowersUP.Remove(Powers);
                    Debug.LogWarning("Power is Empty");
                    return;
                }
            }
        }
    }

    protected virtual void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Vector3 bulletDir = transform.right;
        bulletDir.y += Random.Range(-original_Param.bloom,original_Param.bloom);
        bulletDir.x += Random.Range(-original_Param.bloom,original_Param.bloom);
        bulletDir.Normalize();
        rb.velocity = bulletDir*original_Param.speed;
    }
    protected virtual void FixedUpdate()
    {
        
        transform.right = rb.velocity.normalized * original_Param.speed;
    }
}
