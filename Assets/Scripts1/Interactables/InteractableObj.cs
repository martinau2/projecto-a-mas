﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class InteractableObj : MonoBehaviour
{
    public float interactTimer = 3;
    float timer;
    [Header ("KeyPrompt TMP")]
    public TextMeshPro keyPrompt;
    public SpriteRenderer buttonPrompt;
    [Header("KeyPrompt Key")]
    public string inputKey = "TEST";
    [Header ("Interactable Events")]
    public UnityEvent onInteract;
    public UnityEvent onLeave;
    public UnityEvent onEnter;
    GameObject player;
    [Header ("KeyPrompt Color")]
    public Color pressedColor;
    public Color normalColor;
    public bool SaveInteractable;
    public bool enterSave = false;
    public GameObject this_RespawnPoint;
    bool fading;
    bool isInside;
    public bool isActive = true;
    public bool keyIsFollowing = true;

    // Start is called before the first frame update
    void Start()
    {
        keyPrompt.color = new Color(normalColor.r,normalColor.g,normalColor.b,0);
        buttonPrompt.color = new Color(normalColor.r, normalColor.g, normalColor.b, 0);
        if (SaveInteractable) {
            void save() {
                GameState_Manager.instance.gameCurrent_Info.SceneRespawnIndex = int.Parse(this_RespawnPoint.name);
                GameState_Manager.instance.SaveGameState();
            }
            if (enterSave)
            {
                onEnter.AddListener(save);
            }
            else {
                onInteract.AddListener(save);
            }
        }
    }
    
    // Update is called once per frame
    void Update()
    {
        if (!isActive) {
            //keyPrompt.gameObject.SetActive(false);
            buttonPrompt.gameObject.SetActive(false);
        }
        if (Input.GetButtonDown(inputKey) && timer <= 0 && isInside == true)
        {
            timer = interactTimer;
            onInteract.Invoke();
            StartCoroutine(KeyPromptDown());

        }
        else { timer -= Time.deltaTime; }
    }
    #region Instancia la logica dentro de el area de interaccion
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!isActive)
            return;
        if (collision.tag == "Player")
        {

            
            Vector3 promptPlace = new Vector3(player.transform.position.x, player.transform.position.y + 4, player.transform.position.z);
            //keyPrompt.transform.position = Vector3.Lerp(keyPrompt.transform.position, promptPlace, 0.2f);
            buttonPrompt.transform.position = Vector3.Lerp(buttonPrompt.transform.position, promptPlace, 0.2f);
            //keyPrompt.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y + 3, Player.transform.position.z);
            isInside = true;
            

        }
        
        
    }
    #endregion
    #region Logica al salir del area de interaccion
    private void OnTriggerExit2D(Collider2D collision)
    {
        isInside = false;
        if (!isActive)
            return;
        if (collision.tag == "Player")
        {
            if (!fading && keyIsFollowing)
            {
                StartCoroutine(KeypromptPopout());
            }
            onLeave.Invoke();
            timer = 0;
        }
    }
    #endregion
    #region Logica al entrar al area de interaccion
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") {
            player = collision.gameObject;
            if (!fading && keyIsFollowing) {
                    StartCoroutine(KeypromptPopin());
            }
            //keyPrompt.transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 3, player.transform.position.z);
            buttonPrompt.transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 3, player.transform.position.z);
            onEnter.Invoke();
        }
    }
    #endregion
    //Apretar la tecla de interaccion instancia un cambio de color
    #region Cambio visual de la keyprompt al apretar el boton
    IEnumerator KeyPromptDown() {
        //keyPrompt.color = pressedColor;
        buttonPrompt.color = pressedColor;
        yield return new WaitForSeconds(0.1f);
        //keyPrompt.color = normalColor;
        buttonPrompt.color = normalColor;

    }
    #endregion
    //Desaparece el effecto visual del keyprompt al salir del area de efecto
    #region Destruye el efecto visual al salir del area
    IEnumerator KeypromptPopout()
    {
        fading = true;
        Color keycolor = keyPrompt.color;
        Color buttoncolor = buttonPrompt.color;
        float a = 1;
        while (buttonPrompt.color.a > 0) {
            a -= 0.1f;
            buttonPrompt.color = new Color(keycolor.r, keycolor.g, keycolor.b, a);
            yield return null;
        }
        //while (keyPrompt.color.a > 0)
        //{
            
        //    a -= 0.1f;
        //    keyPrompt.color = new Color(keycolor.r, keycolor.g, keycolor.b, a);
        //    yield return null;
        //}
        fading = false;

    }
    #endregion
    //Muestra el efecto visual al entrar al area
    #region Instancia el efecto visual al entrar al area
    IEnumerator KeypromptPopin()
    {
        if (!isActive)
            yield break;
        fading = true;
        keyPrompt.gameObject.SetActive(true);
        buttonPrompt.gameObject.SetActive(true);
        //Color keycolor = keyPrompt.color;
        Color keycolor = buttonPrompt.color;
        float a = 0;
        while (buttonPrompt.color.a < 1){
            a += 0.1f;
            buttonPrompt.color = new Color(keycolor.r, keycolor.g, keycolor.b, a);
            yield return null;
        }
        //while (keyPrompt.color.a < 1)
        //{
            
        //    a += 0.1f;
        //    keyPrompt.color = new Color(keycolor.r, keycolor.g, keycolor.b, a);
        //    yield return null;
        //}
        fading = false;

    }
    #endregion
    public void SwitchActive()
    {
        isActive = !isActive;
    }
}
