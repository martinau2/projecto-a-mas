﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using TMPro;

public class TextReader : MonoBehaviour
{
    
    public TextMeshPro publicText;
    public TextAsset Text;
    public string currentText = "";
    public float delay;
    public bool doingLine;
    public string[] linesInFile;
    public int currentLine = 0;
    public GameObject dots;
    public bool dotson;
    bool skip;

    public SerializableEvent doneReading;
    

    // Start is called before the first frame update
    void Start()
    {
        dots.SetActive(dotson ? true : false);
        readTextFileLines();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #region Llama la logica del texto
    public void CallText()
    {
        //Activa el TMP que contiene el Texto
        dots.SetActive(dotson ? true:false);
        publicText.enabled = true ;
        //Pregunta si ya hay una linea escribiendose en pantalla
        if (!doingLine)
        {
            //Limpia el texto empieza la corutina que escribe la linea de texto actual y luego hace un loop para saber si ya escribio todo
            publicText.text = "";
            doingLine = true;
            StartCoroutine(ReadLine(linesInFile[currentLine]));
            if (currentLine == linesInFile.Length - 1)
            {
                currentLine = 0;
                doneReading.Raise();
            }
            else
            {
                currentLine++;
            }
        }
        else {
            skip = true;
            print("skip");
        }
    }
    #endregion
    #region Logica de texto en un objecto Interactuable al salir del area
    public void onInteractableLeave() {
        dots.SetActive(false);
        publicText.text = "";
        publicText.enabled = false;
        currentLine = 0;
    }
    #endregion
    #region Corta el archivo .txt en lineas
    void readTextFileLines()
    {
        linesInFile = Text.text.Split('\n');
    }
    #endregion
    #region Escribe una linea del archivo .txt caracter por caracter
    IEnumerator ReadLine(string reading) {
        while (doingLine)
        {
            for (int i = 0; i <= reading.Length; i++)
            {
                if (!skip)
                {
                    currentText = reading.Substring(0, i);
                    publicText.text = currentText;
                    yield return new WaitForSeconds(delay);
                }
                else
                {
                    publicText.text = reading;
                    break;
                }
            }
            doingLine = false;
            skip = false;
            yield return null;
        }
    }
    #endregion
}
