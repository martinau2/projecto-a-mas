﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class GenericEvents
{}
//No Value Events Class
[System.Serializable]
public class UnityEventNoInput : UnityEvent { }

//Float Events class
[System.Serializable]
public class UnityEventFloat : UnityEvent<float> { }

//Int Events class
[System.Serializable]
public class UnityEventInt : UnityEvent<int> { }

//string Events class
[System.Serializable]
public class UnityEventString : UnityEvent<string> { }


//sound Events class
[System.Serializable]
public class UnityEventSoundFX : UnityEvent<Sound_FX> { }


