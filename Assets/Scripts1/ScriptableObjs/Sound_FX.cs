﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SoundFX", menuName = "Sound/SoundFX")]
public class Sound_FX : ScriptableObject
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;

    public AudioClip[] clip;
    public float minPitch;
    public float maxPitch;
    public float minVolume;
    public float maxVolume;
    public AudioClip Song;
    

    public Audio_Message GenerateRandomClip() {
        Audio_Message soundFX_ = new Audio_Message();
        var clipnumber = Random.Range(0, clip.Length);
        if (clip[clipnumber] == null) {
            Debug.LogError("No Sound Detected At Clip Number:" + clipnumber + "/n Currently Playing"+name);
        }
        soundFX_.clip_ = clip[clipnumber];
        soundFX_.pitch_ = Random.Range(minPitch, maxPitch);
        soundFX_.volume_ = Random.Range(minVolume, maxVolume);
        return soundFX_;
    }
}
public struct Audio_Message
{
    public AudioClip clip_;
    public float pitch_;
    public float volume_;

}
