﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="PlayerState",menuName ="Player/PlayerState")]
public class PlayerState : ScriptableObject
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    [Header("Player Health Parameters")]
    public int initial_MaxHealth;
    public int initial_Health;
    [Header("Player Slowmo Values")]
    public float initial_MaxSlowmo;
    public float initial_Slowmo;
    public float initial_RechargeRate;
    [Header("Velocidad de movimiento:")]
    public float initial_Speed;
    [Header("Fuerza de Salto:")]
    public float initial_JumpForce;
    [Header("Tipo de Bala:")]
    public BulletType initial_Bullet;
    [Header("PowerUps:")]
    public PlayerPowerUps initial_PowerUps;

    [Header("Player Health Parameters")]
    public int maxHealth;
    public int health;
    [Header("Player Slowmo Values")]
    public float maxSlowmo;
    public float slowmo;
    public float rechargeRate;
    [Header("Velocidad de movimiento:")]
    public float speed;
    [Header("Fuerza de Salto:")]
    public float jumpForce;
    [Header("Tipo de Bala:")]
    public BulletType bullet;
    [Header("PowerUps:")]
    public PlayerPowerUps powerUps;




    [ContextMenu("Restore")]
    public void restore() {
        maxHealth = initial_MaxHealth;
        health = initial_Health;
        speed = initial_Speed;
        jumpForce = initial_JumpForce;
        bullet = initial_Bullet;
        powerUps = initial_PowerUps;
        slowmo = initial_Slowmo;
        maxSlowmo = initial_MaxSlowmo;
        rechargeRate = initial_RechargeRate;


    }
    
}
