﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Listeners/FloatListener", 0)]
[System.Serializable]
public class FloatListener : MonoBehaviour
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    public SerializableFloatEvent Event;
    public UnityEventFloat Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);

    }
    private void OnDisable()
    {
        Event.DeRegisterListener(this);
    }
    public void OnEventRaised(float dmg)
    {
        Response.Invoke(dmg);
    }
}
