﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Listeners/SFXListener", 0)]
[System.Serializable]
public class SFXListener : MonoBehaviour
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    public SerializableSFXEvent Event;
    public UnityEventSoundFX Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);

    }
    private void OnDisable()
    {
        Event.DeRegisterListener(this);
    }
    public void OnEventRaised(Sound_FX Sound)
    {
        Response.Invoke(Sound);
    }
}
