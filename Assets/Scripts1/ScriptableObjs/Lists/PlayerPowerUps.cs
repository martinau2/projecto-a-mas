﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
[CreateAssetMenu(fileName = "PowerUpList", menuName = "PlayerPowerUpList")]
public class PlayerPowerUps : ScriptableObject
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    public List<PowerUps> bulletpowersUP;
    public List<PowerUps> statsPowerUP;
    [Header("Update Power Ups")]
    public SerializableEvent updatePUP;

    public void AddBulletPowerUp (PowerUps PowerUp){
        bulletpowersUP.Add(PowerUp);
    }
    public void RemoveBulletPowerUp(PowerUps PowerUp)
    {
        bulletpowersUP.Remove(PowerUp);
    }
    public void AddStatsPowerUp (PowerUps PowerUp){
        statsPowerUP.Add(PowerUp);
        updatePUP.Raise();
    }
    public void RemoveStatsPowerUp(PowerUps PowerUp)
    {
        statsPowerUP.Remove(PowerUp);
    }
    public void ClearPowerUps() {
        bulletpowersUP.Clear();
        statsPowerUP.Clear();
    }
}
