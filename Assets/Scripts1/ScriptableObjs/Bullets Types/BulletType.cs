﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "BulletType", menuName = "Bullets/Bullet")]
public class BulletType : ScriptableObject
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    [Header("Dmg Parameters")]
    public int initialDmg;
    public float initialFireRate;
    public float initialSpeed;
    public Bullet initialBullet;
    public LayerMask initialTargetLayer;
    [Range(0,0.1f)]public float initialbloom;

    [HideInInspector] public int dmg;
    [HideInInspector] public float fireRate;
    [HideInInspector] public float speed;
    [HideInInspector] public Bullet bullet;
    [HideInInspector] public LayerMask targetLayer;
    [HideInInspector] public float bloom;

    public void Init(int dmg_, float firerate_, float speed_, Bullet bullet_, LayerMask target_,float bloom_)
    {
        initialDmg = dmg_;
        initialFireRate = firerate_;
        initialSpeed = speed_;
        initialBullet = bullet_;
        initialTargetLayer = target_;
        initialbloom = bloom_;
    }
    public static BulletType CreateInstanceExtension(int dmg_, float firerate_, float speed_, Bullet bullet_, LayerMask target_,float bloom_)
    {
        var data = CreateInstance<BulletType>();
        data.Init(dmg_, firerate_, speed_, bullet_, target_,bloom_);
        return data;
    }

    public void restore() {
        dmg = initialDmg;
        fireRate = initialFireRate;
        speed = initialSpeed;
        bullet = initialBullet;
        targetLayer = initialTargetLayer;
        bloom = initialbloom;
    }
}
