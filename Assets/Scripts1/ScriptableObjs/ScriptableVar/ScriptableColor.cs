﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Color", menuName = "Variables/Color")]
public class ScriptableColor : ScriptableObject
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    public Color variable;
}
