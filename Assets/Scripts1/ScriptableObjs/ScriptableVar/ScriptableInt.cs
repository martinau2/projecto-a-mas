﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Int", menuName = "Variables/Int")]
public class ScriptableInt : ScriptableObject
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    public int variable;
    public SerializableIntEvent OnValueChanged;

    public void SetValue(int Idx) {
        variable = Idx;
        OnValueChanged.Raise(variable);
    }
}
