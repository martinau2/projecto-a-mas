﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Bool",menuName ="Variables/Bools")]
public class ScriptableBools : ScriptableObject
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    public bool variable;

    public void VariableTrue() {
        variable = true;
    }
    public void VariableFalse() {
        variable = false;
    }
}
