﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Float",menuName ="Variables/Float")]
public class ScriptableFloats : ScriptableObject
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    public float variable;

    public void SetFloat(float amount) {
        variable = amount;
    }
}
