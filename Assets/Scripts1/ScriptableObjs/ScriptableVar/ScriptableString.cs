﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "String", menuName = "Variables/String")]
public class ScriptableString : ScriptableObject
{ 
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    public string variable;

    public string returnvar() {
        return variable;

    }
    public void setvar(string var_) {
        variable = var_;

    }
}
