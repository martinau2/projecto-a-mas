﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Event", menuName = "Events/Event")]
public class SerializableEvent : ScriptableObject
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    private List<Listener> listeners = new List<Listener>();
    [ContextMenu("Raise")]
    public void Raise()
    {

        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised();

        }
    }
    public void RegisterListener(Listener listener)
    {
        if (!listeners.Contains(listener))
        {
            listeners.Add(listener);
        }
        else { return; }
    }
    public void DeRegisterListener(Listener listener)
    {
        if (listeners.Contains(listener))
        {
            listeners.Remove(listener);
        }
        else { return; }
    }
}
