﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SFXEvent", menuName = "Events/SFXEvent")]
public class SerializableSFXEvent : ScriptableObject
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;

    private List<SFXListener> listeners = new List<SFXListener>();

    public void Raise(Sound_FX SoundFX)
    {

        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised(SoundFX);

        }
    }
    public void RegisterListener(SFXListener listener)
    {
        if (!listeners.Contains(listener))
        {
            listeners.Add(listener);
        }
        else { return; }
    }
    public void DeRegisterListener(SFXListener listener)
    {
        if (listeners.Contains(listener))
        {
            listeners.Remove(listener);
        }
        else { return; }
    }

}
