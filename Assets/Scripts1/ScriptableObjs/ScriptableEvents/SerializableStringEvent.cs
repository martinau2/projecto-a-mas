﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "StringEvent", menuName = "Events/StringEvent")]
public class SerializableStringEvent : ScriptableObject
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    private List<StringListener> listeners = new List<StringListener>();

    public void Raise(string string_)
    {

        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised(string_);

        }
    }
    public void RegisterListener(StringListener listener)
    {
        if (!listeners.Contains(listener))
        {
            listeners.Add(listener);
        }
        else { return; }
    }
    public void DeRegisterListener(StringListener listener)
    {
        if (listeners.Contains(listener))
        {
            listeners.Remove(listener);
        }
        else { return; }
    }
}