﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    public SerializableSFXEvent launchSound;

    public void PlaySound(Sound_FX Sound) {
        launchSound.Raise(Sound);

    }
}
