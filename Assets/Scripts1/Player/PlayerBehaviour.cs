﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    #region Parameters
    private Rigidbody2D rb;

    public ParticleSystem shotParticle;

    [HideInInspector]
    public bool isGrounded;
    private bool isJumping;

    [HideInInspector]
    public Animator animator;


    [Header("Transforms:")]
    public Transform feet;
    public Transform GunNuzzle;
    [Header("LayerMasks:")]
    public LayerMask groundLayer;
    public LayerMask plataform;
    [SerializeField]
    [Header("Direccion de Mov:")]
    private float moveInput;
    private float aimInput;
    [Header("Radio de Grounded:")]
    public float checkRadius;
    private float jumpTimeCounter;
    [Header("Tiempo de Salto Extra:")]
    public float jumpTime;
    public PlayerState stats;
    [HideInInspector]
    public float currentDelay;
    public bool dead = false;
    bool isTimeSlow = false;
    bool slowmoActive = true;
    public SerializableEvent slomoActivated;

    #endregion

    void Start()
    {
        animator = GetComponent<Animator>();
        stats.restore();
        rb = GetComponent<Rigidbody2D>();
        UpdatePowerUps();
    }


    void Update()
    {
        AnimationCheckUpdate();
        if (dead)
        {
            return;
        }
        checkGround();
        AttackBehaviour();
        if (slowmoActive)
        {
            SlowMotionManagement();
        }
        if (Input.GetButtonDown("TEST"))
        {
            SlowMotionCheck();
        }

    }

    private void FixedUpdate()
    {
        if (dead) {
            return;
        }
        Movement(stats.speed);
    }

    public void StopMoving() {
        rb.velocity = Vector3.zero;
    }
    public void TurnSlowmo() {
        slowmoActive = !slowmoActive;
    }

    #region Slowmo Management
    public void StopSlowmo() {
        ReturnToNormalTime();
    }
    public void SlowMotionCheck() {
        
        if (stats.slowmo == stats.maxSlowmo)
        {
            slomoActivated.Raise();
            isTimeSlow = true;
        }
        else if (stats.slowmo < stats.maxSlowmo && !isTimeSlow)
        {
            ReturnToNormalTime();
        }
        else if (stats.slowmo < stats.maxSlowmo && isTimeSlow)
        {
            ReturnToNormalTime();
        }
    }
    public void SlowMotionManagement() {
        if (isTimeSlow)
        {
            Time.timeScale = 0.2f;
            Time.fixedDeltaTime = Time.timeScale * 0.02f;
            stats.slowmo -= 2*Time.timeScale;
        }
        else if (stats.slowmo < stats.maxSlowmo) {
            stats.slowmo += stats.rechargeRate*Time.timeScale;
            stats.slowmo = Mathf.Clamp(stats.slowmo, 0, stats.maxSlowmo);
        }
        if (stats.slowmo <= 0)
        {
            stats.slowmo = 0;
            ReturnToNormalTime();
        }

    }

    private void ReturnToNormalTime()
    {
        slomoActivated.Raise();
        isTimeSlow = false;
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02f;
    }
    #endregion


    private void AnimationCheckUpdate()
    {
        animator.SetFloat("_YSpeed", rb.velocity.normalized.y);
        animator.SetBool("NotGrounded", !isGrounded);
        animator.SetFloat("_MovInput", Mathf.Abs(Mathf.Ceil(moveInput)));
        if (Input.GetButton("DiagInput") && isGrounded) {
            animator.SetFloat("_Diagonal", 1);
            animator.SetFloat("_InputFire", 0);
        }
        else if (aimInput > 0)
        {
            animator.SetFloat("_InputFire", 1);
        }
        else if (aimInput <= 0 && Input.GetButton("Fire1"))
        {
            animator.SetFloat("_InputFire", -1);
        }
        else if (aimInput <= 0)
        {
            animator.SetFloat("_InputFire", 0);
        }
        if (Input.GetButtonUp("DiagInput")) {
            animator.SetFloat("_Diagonal", 0);
        }
    }
    public void AnimationDeath() {
        animator.SetTrigger("Die");
        dead = true;
        StopMoving();
    }



    #region Movement
    public void Movement(float speed) {
        moveInput = Input.GetAxisRaw("Horizontal");
        rb.velocity = new Vector2(moveInput * Mathf.Clamp(speed,0,40), rb.velocity.y);
        if (moveInput < 0) {
            transform.rotation = Quaternion.Euler(new Vector3(0, 180,0));
        }
        if (moveInput > 0) {
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        }
        aimInput = Input.GetAxisRaw("Vertical");
        if (aimInput > 0)
        {
            GunNuzzle.rotation = Quaternion.Euler(new Vector3(0, GunNuzzle.rotation.eulerAngles.y, 90));
        }
        else if (aimInput <= 0)
        {
            GunNuzzle.rotation = Quaternion.Euler(new Vector3(0, GunNuzzle.rotation.eulerAngles.y, 0));
        }
        

    }
    #endregion
    #region Jump
    public void checkGround() {
        bool isJumpingPress = Input.GetButtonDown("Jump");
        bool isJumpingUP = Input.GetButtonUp("Jump");
        isGrounded = Physics2D.OverlapCircle(feet.position, checkRadius, groundLayer);

        if (rb.velocity.y < 0) {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (2.5f - 1) * Time.deltaTime;
        }
        else if(rb.velocity.y > 0 && !Input.GetButton("Jump")){
            rb.velocity += Vector2.up * Physics2D.gravity.y * (2.0f - 1) * Time.deltaTime;
        }


        if (isGrounded == true && isJumpingPress && aimInput >= 0)
        {
            jumpTimeCounter = jumpTime;
            rb.AddForce(new Vector2(rb.velocity.x, Vector2.up.y * (120)),ForceMode2D.Impulse);
            isJumping = true;
        }
        if (Input.GetButton("Jump") && aimInput >= 0)
        {
            if (jumpTimeCounter > 0 && isJumping == true)
            {
                if (!isTimeSlow)
                {
                    rb.AddForce(new Vector2(rb.velocity.x, Vector2.up.y * (stats.jumpForce))*Time.deltaTime, ForceMode2D.Impulse);
                }
                else {
                    rb.AddForce(new Vector2(rb.velocity.x, Vector2.up.y * (stats.jumpForce*1.3f)*Time.deltaTime), ForceMode2D.Impulse);
                }
                //rb.velocity = new Vector2(rb.velocity.x, Vector2.up.y * (stats.jumpForce));
                jumpTimeCounter -= Time.deltaTime;
            }
            else {
                isJumping = false;
            }
        }
        else if (aimInput < 0 && Input.GetButtonDown("Jump"))
        {
            if (Physics2D.OverlapCircle(feet.position, checkRadius, plataform))
            {
                print("Down Jump");
                Collider2D plataform_ = Physics2D.OverlapCircle(feet.position, checkRadius, plataform);
                StartCoroutine(PhaseDown(plataform_));
            }
        }
        if (isJumpingUP) {
            isJumping = false;
        }
    }

    IEnumerator PhaseDown(Collider2D plataform) {
        plataform.enabled = false;
        yield return new WaitForSeconds(0.3f);
        plataform.enabled = true;
    }
    #endregion
    public void AttackBehaviour() {
        bool isAttaking = Input.GetButton("Fire1");
        if (isAttaking && currentDelay <= 0)
        {
            shotParticle.Play();
            //Fix Temporal porque no quiere andar bien el unity
            if (Input.GetButton("DiagInput") && isGrounded)
            {
                Instantiate(stats.bullet.initialBullet, GunNuzzle.position, Quaternion.Euler(0, 0, transform.rotation == Quaternion.Euler(new Vector3(0, 0, 0)) ? 45 : 135));
            }
            else
            {
                //Esta linea sola deberia funcionar bien pero unity no quiere ser bueno
                Instantiate(stats.bullet.initialBullet, GunNuzzle.position, GunNuzzle.rotation);
            }
            currentDelay = stats.bullet.fireRate;
        }
        else {
            currentDelay -= Time.deltaTime;
        }

    }


    [ContextMenu("Update Power Ups")]
    public void UpdatePowerUps()
    {
        stats.restore();
        if (stats.powerUps.statsPowerUP != null)
        {
            foreach (var Powers in stats.initial_PowerUps.statsPowerUP)
            {
                if (Powers != null)
                    Powers.ApplyPowerUp(null,stats);
                else
                {
                    stats.powerUps.statsPowerUP.Remove(Powers);
                    Debug.LogWarning("Stats Power Up Missing");
                    UpdatePowerUps();
                    return;
                }
            }
        }
    }


    
}



/* public void AttackBehaviour() {
        bool isCharging = Application.platform == RuntimePlatform.Android ? isChargingAndroid.variable : Input.GetButton("Fire1");
        bool isChargingUp = Application.platform == RuntimePlatform.Android ? isChargingAndroidUp.variable : Input.GetButtonUp("Fire1");
        if (isCharging)
        {
            chargeTimer += Time.deltaTime;
            if (!ChargeRay.isPlaying && chargeTimer < chargeTime)
            {
            ChargeRay.Play();
            }
            
        }
        if (chargeTimer > chargeTime) {
            if (!ChargedAttck.isPlaying)
            {
                if (ChargedAttck != null)
                    ChargedAttck.Play();
            }
        }
        if (isChargingUp && (chargeTimer > chargeTime))
        {
            StartCoroutine(Shoot(10,1,1,true));
            chargeTimer = 0;
            if (ChargedAttck != null)
                ChargedAttck.Stop();
            isChargingAndroidUp.variable = false;

        }
        else if (isChargingUp && (chargeTimer < chargeTime))
        {
            StartCoroutine(Shoot(3,0.4f,0.3f,false));
            chargeTimer = 0;
            ChargeRay.Stop();
            isChargingAndroidUp.variable = false;


        }

    }
#region ShootRay
    IEnumerator Shoot(float damage,float laserWidhtStr,float laserWidhtEnd,bool charged) {
        RaycastHit2D Laser = Physics2D.Raycast(GunNuzzle.position, GunNuzzle.right,100,rayHit);
        LaserRay.startWidth = laserWidhtStr;
        LaserRay.endWidth = laserWidhtEnd;
        
        if (Laser)
        {
            
            Laser.collider.SendMessage("TakeDmg", charged == true ? player_Dmg.maxDmg:player_Dmg.minDmg, SendMessageOptions.DontRequireReceiver);
            LaserRay.SetPosition(0, new Vector3(GunNuzzle.position.x,GunNuzzle.position.y,0));
            LaserRay.SetPosition(1, Laser.point);
            

        }
        else {
            LaserRay.SetPosition(0, new Vector3(GunNuzzle.position.x, GunNuzzle.position.y, 0));
            LaserRay.SetPosition(1, new Vector3(GunNuzzle.position.x, GunNuzzle.position.y, 0) + GunNuzzle.right * 100);
        }
        LaserRay.enabled = true;
        yield return new WaitForSeconds(0.3f);
        LaserRay.enabled = false;
    }
    #endregion
*/