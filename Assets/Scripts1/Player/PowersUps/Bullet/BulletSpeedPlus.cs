﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="SpeedUp+",menuName ="PowerUps/Bullet/Speed+")]
public class BulletSpeedPlus : PowerUps
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;

    public override void AddPowerUp(PlayerPowerUps _List)
    {
        _List.AddBulletPowerUp(this);
    }

    public override void ApplyPowerUp(BulletType bulletParam_,PlayerState not = null)
    {
        bulletParam_.speed *= multiplayer;
    }


}
