﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SlowmoExtend", menuName = "PowerUps/Stats/SlowmoExtend")]
public class SlowmoExtended : PowerUps
{
    public override void AddPowerUp(PlayerPowerUps _List)
    {
        _List.AddStatsPowerUp(this);
    }

    public override void ApplyPowerUp(BulletType PowerUPBullet = null, PlayerState PowerUPStats = null)
    {
        PowerUPStats.maxSlowmo += multiplayer;
        PowerUPStats.slowmo = PowerUPStats.maxSlowmo;
    }
}
