﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpeedUp+", menuName = "PowerUps/Stats/Speed+")]
public class SpeedUp : PowerUps
{
    public override void AddPowerUp(PlayerPowerUps _List)
    {
        _List.AddStatsPowerUp(this);
    }

    public override void ApplyPowerUp(BulletType PowerUPBullet = null, PlayerState PowerUPStats = null)
    {
        PowerUPStats.speed *= multiplayer;
    }
}
