﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class PowerUps : ScriptableObject
{
    public float multiplayer;
    public string powerUpName;
    public Sprite powerUpSprite;
    public Sprite smallPowerSprite;

    public abstract void ApplyPowerUp(BulletType PowerUPBullet = null,PlayerState PowerUPStats = null);
    public abstract void AddPowerUp(PlayerPowerUps _List);

}
//public abstract class PowerUps<T> : ScriptableObject
//{
//    public float multiplayer;

//    public abstract void ApplyPowerUp(T PowerUP);

//}
//public abstract class BulletPowerUps : PowerUps<BulletType> {

//    public override abstract void ApplyPowerUp(BulletType PowerUP);
//}

//public abstract class PlayerStatsPowerUps : PowerUps<PlayerState> {

//    public override abstract void ApplyPowerUp(PlayerState PowerUP);
//}

