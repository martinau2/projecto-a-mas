﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{

    public SpriteRenderer Sprite;
    public bool invulnerability = false;
    public PlayerState stats;

    public Material material;
    [Space(25)]
    public float inmunityTimer = 0.5f;
    [ColorUsage(true, true)]
    public Color playerColor;
    [ColorUsage(true, true)]
    public Color hitColor;
    public SerializableIntEvent hudHP;
    public SerializableEvent deathEvent;

    [Header("Sound FXs´")]
    public SoundPlayer sp;
    public Sound_FX hit;
    [ContextMenu("Update Color")]
    private void OnValidate()
    {
        material.SetColor("_MainColor", playerColor);
    }
    private void Awake()
    {
        material.SetColor("_MainColor",playerColor);


    }
    public void Start()
    {
        

    }
    public void Update()
    {
        
    }

    public void RecibeDamage(int dmg)
    {
        if (invulnerability) {
            return;
        }
        hudHP.Raise(dmg);
        if (stats.health <= 0) {
            deathEvent.Raise();
            return;
        }
        print("Perdiste:" + dmg);
        sp.PlaySound(hit);
        StartCoroutine(DamageFade());
        StartCoroutine(DamageInmunity());
    }
  
    IEnumerator DamageFade()
    {
        float timer = inmunityTimer;
        while (timer >= 0)
        {
            timer -= Time.deltaTime+((inmunityTimer/4)*2);
            material.SetColor("_MainColor", hitColor);
            yield return new WaitForSeconds(inmunityTimer/4);
            material.SetColor("_MainColor", playerColor);
            yield return new WaitForSeconds(inmunityTimer / 4);


        }
        material.SetColor("_MainColor", playerColor);
    }
    public IEnumerator DamageInmunity() {

        invulnerability = true;
        
            
            float timer = inmunityTimer;
            
       while (timer >= 0)
       {
                
        

        timer -= Time.deltaTime;

        yield return null;
                
      }
      if (timer <= 0)
      {
        
        timer -= Time.deltaTime;
        invulnerability = false;
        yield return null;
      }
        
        
        
    }
    
}
