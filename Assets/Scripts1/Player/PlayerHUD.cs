﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Rendering.PostProcessing;

public class PlayerHUD : MonoBehaviour
{
    public Image hpBar_;
    [SerializeField]
    //private int currentHealth;
    //public int maxHealth;
    public PlayerState stats;
    public Image[] powerUpsHolders;
    public TextMeshProUGUI[] powerUpsAmount;
    public int[] powerUpsAmountNumber;
    public PlayerPowerUps powerUps;
    
    public Image slowmo;
    public Image slowmoBG;


    public Image playerHeadGUI;

    public GameObject bossGUI;
    public Image bossHealth;
    public Boss currentBoss;
    [Range(0,1)]public float bossTresh;
    float bossMaxHealth;


    public TextMeshProUGUI slowmoPRCNT;
    public ParticleSystem slowmoReady;
    Color slowmoColorGUI;
    Color slowmoBGColorGUI;
    [ColorUsage(true,true)]
    public Color hitColor;
    [ColorUsage(true,true)]
    public Color normalColor;

    ParticleSystem.MainModule settings;

    public PostProcessProfile profile;
    public ScriptableColor profileInitialColor;
    Vignette vig = null;

    private void OnValidate()
    {
        if (profile != null)
        {
        profile.TryGetSettings(out vig);
        vig.color.value = profileInitialColor.variable;
        vig.intensity.value = 0.1f;
        }
    }

    private void Awake()
    {
        slowmoReady.gameObject.SetActive(true);
        UpdateHudPowerUps();
        settings = slowmoReady.main;
        slowmoColorGUI = slowmo.color;
        slowmoBGColorGUI = slowmoBG.color;
        playerHeadGUI.material.SetColor("_MainColor", normalColor);
        if (profile != null)
        {
            profile.TryGetSettings(out vig);
            vig.color.value = profileInitialColor.variable;
            vig.intensity.value = 0.1f;
        }
    }
    private void Update()
    {
        if (currentBoss != null && bossGUI.activeSelf) {
            bossHealth.fillAmount = currentBoss.bossHealth / bossMaxHealth;
            if (currentBoss.bossHealth / bossMaxHealth <= bossTresh) {
                bossHealth.color = Color.red;
            }
        }


        float slowmoFill = stats.slowmo / stats.maxSlowmo;
        slowmo.fillAmount = slowmoFill;
        slowmoPRCNT.text = slowmoFill.ToString("000%");
        if (slowmoFill == 1)
        {
            slowmoReady.gameObject.SetActive(true);
            settings.startColor = new ParticleSystem.MinMaxGradient(Color.yellow);

        }
        else {
            slowmoReady.gameObject.SetActive(false);
        }
        slowmo.color = Color.Lerp(slowmoColorGUI,Color.red,1-slowmoFill);
        slowmoBG.color = Color.Lerp(slowmoBGColorGUI,new Color32(132, 0, 0, 132),1 -slowmoFill);
        vig.color.value = Color.Lerp(profileInitialColor.variable, Color.red, 1 - ((float)stats.health / stats.maxHealth));
        vig.intensity.value = Mathf.Lerp(0.1f, 0.35f, 1 - ((float)stats.health / stats.maxHealth));

    }
    //Updates the GUI for power UPS
    public void UpdateHudPowerUps()
    {
        foreach (var powerUP in powerUps.bulletpowersUP)
        {
            for (int i = 0; i < powerUpsHolders.Length; i++)
            {
                if (powerUpsHolders[i].sprite == powerUP.smallPowerSprite)
                {
                    powerUpsAmountNumber[i] += 1;
                    powerUpsAmount[i].text = "X" + powerUpsAmountNumber[i];
                    break;
                }
                else if (powerUpsHolders[i].sprite == null)
                {
                    powerUpsHolders[i].sprite = powerUP.smallPowerSprite;
                    powerUpsHolders[i].color = Color.white;
                    break;
                }
            }

        }
        foreach (var powerUP in powerUps.statsPowerUP)
        {
            for (int i = 0; i < powerUpsHolders.Length; i++)
            {
                if (powerUpsHolders[i].sprite == powerUP.smallPowerSprite)
                {
                    powerUpsAmountNumber[i] += 1;
                    powerUpsAmount[i].text = "X" + powerUpsAmountNumber[i];
                    break;
                }
                else if (powerUpsHolders[i].sprite == null)
                {
                    powerUpsHolders[i].sprite = powerUP.smallPowerSprite;
                    powerUpsHolders[i].color = Color.white;
                    break;
                }
            }

        }
    }
    //Clears the GUI for power UPS
    public void ClearPowerUpsGUI() {
        for (int i = 0; i < powerUpsHolders.Length; i++)
        {
            powerUpsHolders[i].sprite = null;
            powerUpsAmountNumber[i] = 1;
            powerUpsAmount[i].text = "";
        }
    }
    //Updates the hp in the GUI
    public void UpdateHP(int dmg) {
        if (stats.health <= 0) {
            stats.health = 0;
            return;
        }
        int _currentHealth = stats.health;
        stats.health -= dmg;
        StartCoroutine(HPModify(_currentHealth));

    }
    public IEnumerator HPModify(int dmg) {
        float _currentHealth = dmg;
        while (_currentHealth > stats.health && _currentHealth > 0) {
            _currentHealth -= 1;
            hpBar_.fillAmount = _currentHealth / stats.initial_MaxHealth;
            playerHeadGUI.material.SetColor("_MainColor", hitColor);
            yield return new WaitForSeconds(0.025f);
            playerHeadGUI.material.SetColor("_MainColor", normalColor);
            yield return new WaitForSeconds(0.025f);
        }
    }

    public void ShowBossGUI() {
        bossGUI.SetActive(true);
        bossMaxHealth = currentBoss.bossHealth;
    }
    public void HideBossGUI() {
        bossGUI.SetActive(false);
        currentBoss = null;
    }
}
