﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PowerUPDrop : MonoBehaviour
{
    public PlayerPowerUps powerUps_Player;
    public PowerUps powerUps_This;
    public TextMeshPro description_This;
    public PowerUps[] powerUpsPool;
    

    public SerializableEvent updatePowerUpsGUI;

    public void ApplyPowerUp()
    {
        powerUps_This.AddPowerUp(powerUps_Player);
        updatePowerUpsGUI.Raise();
    }

    public void GenerateRandomPowerUP() {
        powerUps_This = powerUpsPool[Random.Range(0, powerUpsPool.Length)];
        description_This.text = powerUps_This.powerUpName;
        GetComponentInChildren<SpriteRenderer>().sprite = powerUps_This.powerUpSprite;
    }
}
