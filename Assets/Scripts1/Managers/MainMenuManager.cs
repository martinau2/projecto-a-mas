﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainMenuManager : MonoBehaviour
{
    public ScriptableFloats SFXVolume, MusicVolume;
    public ScriptableFloats ScreenHeight, ScreenWidth,currentres;
    public bool fullRes;
    public Slider SFXBar, MusicBar;
    public TMP_Dropdown ResolutionHandle;
    public Button fullscreenOn, fullscreenOff;
    public TextMeshProUGUI SFXPercent, MusicPercent;
    public List<ScriptableString> savegames;
    public SerializableEvent ChangedRes;
    public Button startingSelectedButton;
    public Button[] mainmenuButtons;


    public Sound_FX menuSong;
    public SerializableSFXEvent playMusic;

    private void Start()
    {
        ChangeScreenRes(Mathf.RoundToInt(currentres.variable));
        SFXBar.value = SFXVolume.variable;
        MusicBar.value = MusicVolume.variable;
        for (int i = 0; i < savegames.Count; i++)
        {
            if (Directory.Exists(savegames[i].variable))
            {
                GameState_Manager.instance.savegamesFiles.Add(savegames[i]);
            }
        }
        startingSelectedButton.Select();
        playMusic.Raise(menuSong);

        
    }
    
    public void ChangeScreenRes(int value)
    {
        CheckResolutionValue(value);
        Screen.SetResolution(Mathf.RoundToInt(ScreenWidth.variable), Mathf.RoundToInt(ScreenHeight.variable), fullRes);
        ChangedRes.Raise();
    }


    public void ChangeFullScreen() {
        fullRes = !fullRes;
        Screen.fullScreen = fullRes;
    }

    private void CheckResolutionValue(int value)
    {
        if (value == 0)
        {
            ScreenHeight.variable = 1080;
            ScreenWidth.variable = 1920;
            currentres.variable = 0;
        }
        else if (value == 1)
        {
            ScreenHeight.variable = 960;
            ScreenWidth.variable = 1280;
            currentres.variable = 1;
        }
        else if (value == 2)
        {
            ScreenHeight.variable = 768;
            ScreenWidth.variable = 1024;
            currentres.variable = 2;
        }
    }


    //Updates Config Options
    public void UpdateConfig()
    {
        SFXBar.value = SFXVolume.variable;
        SFXPercent.text = SFXVolume.variable.ToString("000" + "%");
        MusicBar.value = MusicVolume.variable;
        MusicPercent.text = MusicVolume.variable.ToString("000" + "%");

        ResolutionHandle.value = Mathf.RoundToInt(currentres.variable);
        fullscreenOff.gameObject.SetActive(!fullRes ? true : false);
        fullscreenOn.gameObject.SetActive(!fullRes ? false : true);
    }
    //Deletes SaveFiles
    public void ErraseAll(ScriptableString savegame) {
        if (GameState_Manager.instance.savegamesFiles.Contains(savegame))
        {
            GameState_Manager.instance.currentSavegameFile = savegame;
            GameState_Manager.instance.DeleteGameState();
            GameState_Manager.instance.savegamesFiles.Remove(savegame);
            GameState_Manager.instance.currentSavegameFile = null;
        }
            //foreach (var savegame in savegames)
            //{
            //    if (GameState_Manager.instance.savegamesFiles.Contains(savegame))
            //    {
            //        GameState_Manager.instance.currentSavegameFile = savegame;
            //        GameState_Manager.instance.DeleteGameState();
            //        GameState_Manager.instance.savegamesFiles.Remove(savegame);
            //        GameState_Manager.instance.currentSavegameFile = null;
            //    }
            //}
    }

    public void EXITGAME() {
        Application.Quit();

    }

    public void CounterActivate() {
        foreach (var item in mainmenuButtons)
        {
            item.enabled = !item.enabled;
        }
    }

}
