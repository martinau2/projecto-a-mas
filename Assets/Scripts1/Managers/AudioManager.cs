﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public AudioSource soundPlayer;
    public MusicPlayer[] musicPlayer;
    private int currentPlayer = 0;
    public Sound_FX testsound;
    public Sound_FX song1;
    public Sound_FX song2;

    Vector2 volumeSFX;

    public AudioMixer audioMixer;
    Audio_Message currentClip;

    public ScriptableFloats soundFXVolume;
    public ScriptableFloats musicVolume;

    public SerializableStringEvent musicVolumeString;
    public SerializableStringEvent sFXVolumeString;
    
    //Wrapper
    public void ChangeSong(Sound_FX Song)
    {
        StartCoroutine(OnMusicPlayed(Song));
    }

    //Wrapper
    public void PlaySound(Sound_FX Sound)
    {
        StartCoroutine(OnSoundPlayed(Sound));
    }
    //TEST
    private void Start()
    {
        audioMixer.GetFloat("SFXVolume", out volumeSFX.x);
        audioMixer.GetFloat("SFXPitch", out volumeSFX.y);
    }


    public void OnChangedVolumeSoundSFX(float volume)
    {
        //takes float and turns it into a logaritmic function to resamble sound db
        audioMixer.SetFloat("SFXVolume", Mathf.Log10(volume)*20);
        var volume_ = (volume/1)*100;
        sFXVolumeString.Raise(volume_.ToString("000")+"%");
        
        audioMixer.GetFloat("SFXVolume", out volumeSFX.x);
        audioMixer.GetFloat("SFXPitch", out volumeSFX.y);

    }
    public void OnChangedVolumeMusic(float volume)
    {
        //takes float and turns it into a logaritmic function to resamble sound db
        audioMixer.SetFloat("MusicVolume", Mathf.Log10(volume)*20);
        var volume_ = (volume / 1) * 100;
        musicVolumeString.Raise(volume_.ToString("000")+"%");
        
    }


    #region Plays a Sound Effect on a new AudioSource
    public IEnumerator OnSoundPlayed(Sound_FX soundFX_) {
        var AudioSource = gameObject.AddComponent<AudioSource>();
        AudioSource.loop = false;
        AudioSource.playOnAwake = false;
        AudioSource.outputAudioMixerGroup = soundPlayer.outputAudioMixerGroup;

        //Set new values
        currentClip = soundFX_.GenerateRandomClip();
        audioMixer.SetFloat("SFXPitch",volumeSFX.y+currentClip.pitch_);
        audioMixer.SetFloat("SFXVolume",volumeSFX.x+currentClip.volume_);
        AudioSource.clip = currentClip.clip_;
        AudioSource.Play();
        while (AudioSource.isPlaying) {
            yield return null;
        }
        Destroy(AudioSource);

        //Restore values
        audioMixer.SetFloat("SFXVolume", volumeSFX.x);
        audioMixer.SetFloat("SFXPitch", volumeSFX.y);
    }
    #endregion

    #region Does a Fade between Songs
    private IEnumerator OnMusicPlayed(Sound_FX soundFX_) {
        int targetPlayer = currentPlayer == 0 ? 1 : 0;
        musicPlayer[targetPlayer].Source.clip = soundFX_.Song;
        musicPlayer[targetPlayer].Source.Play();

        //Get current values
        Vector2 volumecurrent;
        audioMixer.GetFloat(musicPlayer[currentPlayer].VolumeTag,out volumecurrent.x);
        audioMixer.GetFloat(musicPlayer[currentPlayer].PitchTag, out volumecurrent.y);

        Vector2 volumetarget;
        audioMixer.GetFloat(musicPlayer[targetPlayer].PitchTag, out volumetarget.y);
        audioMixer.SetFloat(musicPlayer[targetPlayer].VolumeTag, -80);
        audioMixer.GetFloat(musicPlayer[targetPlayer].VolumeTag, out volumetarget.x);

        while (volumecurrent.x != -80) {
            audioMixer.SetFloat(musicPlayer[currentPlayer].VolumeTag, volumecurrent.x -1);
            audioMixer.GetFloat(musicPlayer[currentPlayer].VolumeTag, out volumecurrent.x);
            

            if (volumetarget.x < 0) {
                audioMixer.SetFloat(musicPlayer[targetPlayer].VolumeTag, volumetarget.x +1);
                audioMixer.GetFloat(musicPlayer[targetPlayer].VolumeTag, out volumetarget.x);
            }
            yield return new WaitForSeconds(0.05f);
        }
        while (volumetarget.x < 0) {
            audioMixer.SetFloat(musicPlayer[targetPlayer].VolumeTag, volumetarget.x + 1);
            audioMixer.GetFloat(musicPlayer[targetPlayer].VolumeTag, out volumetarget.x);
            yield return new WaitForSeconds(0.05f);
        }
        currentPlayer = targetPlayer;
        
    }
    #endregion

    [System.Serializable]
    public struct MusicPlayer {
        public string VolumeTag;
        public string PitchTag;
        public AudioSource Source;
    }
}
