﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InGameMenuManager : MonoBehaviour
{
    public ScriptableFloats SFXVolume, MusicVolume;
    public ScriptableFloats ScreenHeight, ScreenWidth, currentres;
    public bool fullRes;
    public Slider SFXBar, MusicBar;
    public TMP_Dropdown ResolutionHandle;
    public Button fullscreenOn, fullscreenOff;
    public TextMeshProUGUI SFXPercent, MusicPercent;
    public SerializableEvent ChangedRes;


    public void ChangeScreenRes(int value)
    {
        CheckResolutionValue(value);
        Screen.SetResolution(Mathf.RoundToInt(ScreenWidth.variable), Mathf.RoundToInt(ScreenHeight.variable), fullRes);
        ChangedRes.Raise();
    }

    private void CheckResolutionValue(int value)
    {
        if (value == 0)
        {
            ScreenHeight.variable = 1080;
            ScreenWidth.variable = 1920;
            currentres.variable = 0;
        }
        else if (value == 1)
        {
            ScreenHeight.variable = 960;
            ScreenWidth.variable = 1280;
            currentres.variable = 1;
        }
        else if (value == 2)
        {
            ScreenHeight.variable = 768;
            ScreenWidth.variable = 1024;
            currentres.variable = 2;
        }
    }
    public void ChangeFullScreen()
    {
        fullRes = !fullRes;
        Screen.fullScreen = fullRes;
    }
    public void UpdateConfig()
    {
        SFXBar.value = SFXVolume.variable;
        SFXPercent.text = SFXVolume.variable.ToString("000" + "%");
        MusicBar.value = MusicVolume.variable;
        MusicPercent.text = MusicVolume.variable.ToString("000" + "%");

        ResolutionHandle.value = Mathf.RoundToInt(currentres.variable);
        fullscreenOff.gameObject.SetActive(!fullRes ? true : false);
        fullscreenOn.gameObject.SetActive(!fullRes ? false : true);
    }

    public void EXITGAME()
    {
        Application.Quit();

    }
}
