﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveScene : MonoBehaviour
{
   
   //Escena a cargar el punto donde deberia spawnear el player y el canvas de fade 
    public int Scenetoload;
    public int SpawnPoint;
    public SerializableEvent transition;
    private bool Loading;
    public float timeBeforeLoad = 2.5f;
    
    

    //Referencias al player para ser instanciado o movido de escena
    public GameObject Player;
    public GameObject PlayerPrefab;
    
    private void Start()
    {
        
        SceneManager.sceneLoaded += OnSceneLoaded;
        //Player = GameObject.FindGameObjectWithTag("Player");
    }
    //Trigger para cambiar de escena
    #region Logica de triggers para cambiar nivel
    public void OnTriggerEnter2D(Collider2D collision)
    {
        //Player = GameObject.FindGameObjectWithTag("Player");
        //Player.GetComponent<PlayerBehaviour>().enabled = false;
        //StartCoroutine(LoadSceneAsyncWithPlayer(Scenetoload,SpawnPoint));
    }
    #endregion
    //Carga una escena nueva con un player existente
    #region Logica para cargar una nueva escena teniendo un Player existente
    public IEnumerator LoadSceneAsyncWithPlayer(int Scene_ToLoad,int Spawn_Point)
    {

        transition.Raise();
        yield return new WaitForSeconds(timeBeforeLoad);
        //Guardar la escena actual
        Scene currentScene = SceneManager.GetActiveScene();

        // carga la escena additivamente
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(Scene_ToLoad, LoadSceneMode.Additive);

        // espera a que cargue la escena
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        
        // mueve al player a la siguiente escena
        SceneManager.MoveGameObjectToScene(Player, SceneManager.GetSceneByBuildIndex(Scene_ToLoad));
        
        LoadPlayer(Spawn_Point,Player);
        // descarga la escena anterior
        SceneManager.UnloadSceneAsync(currentScene);
    }
    #endregion
    //Carga una escena nueva que no necesita el player 
    #region Logica para cargar una nueva escena sin necesitar un Player
    public IEnumerator LoadSceneAsyncEmpty(int Scene_ToLoad)
    {

        transition.Raise();
        yield return new WaitForSeconds(timeBeforeLoad);
        //Guardar la escena actual
        Scene currentScene = SceneManager.GetActiveScene();

        // carga la escena additivamente
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(Scene_ToLoad, LoadSceneMode.Additive);

        // espera a que cargue la escena
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        

        
        // descarga la escena anterior
        SceneManager.UnloadSceneAsync(currentScene);
    }
    #endregion
    //Carga la misma escena y instancia al player
    #region Logica para recargar la escena actual instanciando un nuevo Player
    public IEnumerator ReloadSceneAsyncWithoutPlayer(int Spawn_Point,int SceneIndx)
    {

        transition.Raise();
        yield return new WaitForSeconds(timeBeforeLoad);
        //Guardar la escena actual
        Scene currentScene = SceneManager.GetActiveScene();

        // carga la escena additivamente
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(SceneIndx, LoadSceneMode.Additive);

        // espera a que cargue la escena
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        //como esta cargando la misma escena, hay que encontrar la nueva escena en la lista de escenas activas
        Scene newcurrentScene = SceneManager.GetSceneAt(1);
        // instancia al Player, le da su punto de spawn luego lo mueve a la nueva escena y le pasa la referencia de el nuevo Player a el gamestate
        Player = InstantiatePlayer(Spawn_Point, PlayerPrefab);
        SceneManager.MoveGameObjectToScene(Player, newcurrentScene);
        Player.SetActive(true);
        gameObject.GetComponent<GameState_Manager>().playerobj = Player;


        // descarga la escena anterior
        SceneManager.UnloadSceneAsync(currentScene);
        
    }
    #endregion
    //Carga una escena nueva sin tener un player existente
    #region Logica para cargar una nueva escena sin tener un Player existente
    public IEnumerator LoadSceneAsyncWithoutPlayer(int Scene_ToLoad, int Spawn_Point)
    {
        if (Loading) { yield break; }
        Loading = true;
        transition.Raise();
        yield return new WaitForSeconds(timeBeforeLoad);
        
        Scene currentScene = SceneManager.GetActiveScene();

        
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(Scene_ToLoad, LoadSceneMode.Additive);

        
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        Scene newcurrentScene = SceneManager.GetSceneAt(1);

        Player = InstantiatePlayer(Spawn_Point, PlayerPrefab);
        SceneManager.MoveGameObjectToScene(Player,newcurrentScene);
        Player.SetActive(true);
        gameObject.GetComponent<GameState_Manager>().playerobj = Player;
        
        



        
        SceneManager.UnloadSceneAsync(currentScene);
        GameState_Manager.instance.gameCurrent_Info.Scene = Scene_ToLoad;
        GameState_Manager.instance.gameCurrent_Info.SceneRespawnIndex = Spawn_Point;
        Loading = false;
    }
    #endregion
    //posiciona al player en el lugar que deberia y se los envia a el gamestate Manager
    #region Encuentra la posicion a la que debe posicionarse el player y lo posiciona
    public void LoadPlayer(int SpawnPoint,GameObject Player) {
        Player.GetComponent<PlayerBehaviour>().enabled = false;
        GameObject Respawn = null;// = GameObject.Find(SpawnPoint.ToString());
        GameObject[] RespawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
        foreach (GameObject SpawnPoints in RespawnPoints)
        {
            if (SpawnPoints.name == SpawnPoint.ToString()) {
                Respawn = SpawnPoints;
            }
        }
        GameState_Manager.instance.gameCurrent_Info.SceneRespawnIndex = SpawnPoint;
        GameState_Manager.instance.gameCurrent_Info.Scene = Scenetoload;
        if (Respawn == null)
        {
            Debug.LogError("Respawn Not Found,look if Scene being loaded is correct");
        }
        else if (Player == null) {
            Debug.LogError("Player Not Found");
        }
        Player.transform.position = Respawn.transform.position;
        Player.transform.rotation = Respawn.transform.rotation;
        Player.transform.localScale = Respawn.transform.localScale;
        Player.GetComponent<PlayerBehaviour>().enabled = true;
        Player.GetComponent<Animator>().Play("Ground Blend Tree");





    }
    #endregion
    //instancia al player y devuelve la referencia a tal
    #region Instancia al Player en la nueva escena a cargar y devuelve la referencia
    public GameObject InstantiatePlayer(int SpawnPoint,GameObject PlayerPrefab) {
        GameObject Respawn = GameObject.Find(SpawnPoint.ToString());
        GameObject Player = Instantiate(PlayerPrefab,Respawn.transform.position,Respawn.transform.rotation,null);
        Player.transform.localScale = Respawn.transform.localScale;
        return Player;

    }
    #endregion
    #region Cada vez que se carga una escena busca al jugador y pasa la referencia a el Gamestate_Manager
    public void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        Player = GameObject.FindGameObjectWithTag("Player");
        GameState_Manager.instance.playerobj = Player;



    }
    #endregion
    //recarga la escena
    public void ReloadScene(int Spawn_Point) {
        StartCoroutine(ReloadSceneAsyncWithoutPlayer(Spawn_Point, SceneManager.GetActiveScene().buildIndex));
        
    }
    public void LoadScene(int Spawn_Point) {
        StartCoroutine(LoadSceneAsyncWithPlayer(Scenetoload,Spawn_Point));
    }
}