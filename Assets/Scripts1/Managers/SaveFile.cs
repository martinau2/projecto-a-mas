﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SaveFile : MonoBehaviour
{
    public ScriptableString fileDirectory;
    public ScriptableInt fileProgression;
    public bool startNewGame = false;


    public Button savefileButton,trashButton;
    public Image firstBoss, secondBoss;
    public TextMeshProUGUI active;

    public void ActivateSaveFiles()
    {
        if (!startNewGame)
        {
            savefileButton.onClick.AddListener(OnSelected);
        }
        else
        {
            savefileButton.onClick.AddListener(OnStartNewGame);
        }
        if (GameState_Manager.instance.savegamesFiles.Contains(fileDirectory))
        {
            savefileButton.enabled = true;
            GameState_Manager.instance.currentSavegameFile = fileDirectory;
            GameState_Manager.instance.LoadGameState(false);
            if (fileProgression.variable == 0)
                firstBoss.enabled = true;
            else if (fileProgression.variable == 1)
            {
                firstBoss.enabled = true;
                secondBoss.enabled = true;
            }
            
            active.text = fileDirectory.variable;

        }
        else if(!startNewGame) {
            savefileButton.gameObject.SetActive(false);
            trashButton.gameObject.SetActive(false);
            active.text = "";
        }
        if (!GameState_Manager.instance.savegamesFiles.Contains(fileDirectory)) {
            active.text = "Empty Slot";
        }
    }
    public void OnSelected() {
        GameState_Manager.instance.currentSavegameFile = fileDirectory;
        GameState_Manager.instance.LoadGameState();
    }
    public void OnStartNewGame() {
        GameState_Manager.instance.currentSavegameFile = fileDirectory;
        GameState_Manager.instance.DeleteGameState();
        GameState_Manager.instance.StartScene();

    }
}
