﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    public Canvas pauseMenu;
    public bool isPaused;
    public ScriptableBools pausedBool;
    float currentTimeScale;
    public bool canPause = true;
    public Button resume;

    public EventSystem eventSystem;

    public void OnPauseTrigger() {
        if (!canPause) {
            return;
        }
        if (isPaused == false)
        {
            pausedBool.variable = true;
            isPaused = true;
            eventSystem.enabled = true;
            currentTimeScale = Time.timeScale;
            Time.timeScale = 0;
            //pauseMenu.enabled = true;
            GetComponent<Animator>().SetTrigger("PauseEnter");
            GetComponent<Animator>().ResetTrigger("PauseExit");
            resume.Select();
        }
        else
        {
            pausedBool.variable = false;
            isPaused = false;
            eventSystem.enabled = false;
            Time.timeScale = currentTimeScale;
            GetComponent<Animator>().SetTrigger("PauseExit");
            GetComponent<Animator>().ResetTrigger("PauseEnter");
            //pauseMenu.enabled = false;
        }


    }

    public void ExitToMenu() {
        Time.timeScale = 1;
        LeavingEarly();
        GameState_Manager.instance.MainMenuScene();

    }
    public void LeavingEarly() {
        GameState_Manager.instance.earlyLeave = true;
    }
    public void CanPause(bool can) {
        canPause = can;
    }

    public void EnableSystemEvents() {
        if (eventSystem != null) {
            eventSystem.enabled = true;
        }
    }
}
