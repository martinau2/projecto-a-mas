﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using TMPro;



public class GameState_Manager : MonoBehaviour
{

    public static GameState_Manager instance { private set; get; }
    [Header("Informacion del Jugador")]
    public PlayerState playerSave_Info;
    [Header("Informacion del Estado de el juego")]
    public GameState gameCurrent_Info;
    [Header("Informacion del Guardado de el juego")]
    public GameState gameSave_Info;
    [Header("Instancia actual del Jugador")]
    public GameObject playerobj;
    [Header("Prefab del Player")]
    public GameObject playerPrefab;
    [Header("Encargado de Precargar y Destruir Escenas")]
    public MoveScene MoveSceneManager;
    [Header("BossFight Ranking")]
    public GameObject rankStats;
    public bool aliveState;
    public bool earlyLeave;


    public SerializableEvent finishScore;
    bool notPausable;
    public SerializableEvent pause;
    public ScriptableBools isPaused;

    public List<ScriptableFloats> configFloats;
    public List<ScriptableObject> objectsToPersist;
    public List<ScriptableString> savegamesFiles;
    public ScriptableString currentSavegameFile;
    public Vector2 scene_SpawnPoint_FirstLevel;

    float timescale;
    public void Awake()
    {
        #region Singleton
        if (instance != null)
        {
            Debug.Log("Error" + this);
            Destroy(this.gameObject);
            return;

        }
        instance = this;
        #endregion
        DontDestroyOnLoad(this);
        LoadSetting();
        
    }
    private void OnApplicationFocus(bool focus)
    {
        if (isPaused.variable) {
            return;
        }
        if (Time.timeScale != 0) {
            timescale = Time.timeScale;
        }
        if (!focus)
        {
            Time.timeScale = 0;
        }
        else {
            Time.timeScale = timescale;
        }

    }
    public void Update()
    {
        if (Input.anyKey)
        {
            if (Input.GetKey(KeyCode.JoystickButton0) || Input.GetKey(KeyCode.JoystickButton1) || Input.GetKey(KeyCode.JoystickButton2) || Input.GetKey(KeyCode.JoystickButton3) || Input.GetKey(KeyCode.JoystickButton4) || Input.GetKey(KeyCode.JoystickButton5) || Input.GetKey(KeyCode.JoystickButton6) || Input.GetKey(KeyCode.JoystickButton7)|| Input.GetKey(KeyCode.JoystickButton8)|| Input.GetKey(KeyCode.JoystickButton9))
            {
                Cursor.visible = false;
            }
            else {
                Cursor.visible = true;
            }
               
        }
        if (notPausable) {
            return;
        }
        if (Input.GetButtonDown("EscapeMenu")) {
            pause.Raise();

        }
    }
    //recarga la escena
    public void Reload() {
     LoadGameState(true);
    }
    #region SaveLogic
    public void SaveGameState() {
        if (!Directory.Exists(currentSavegameFile.variable))
        {
            Directory.CreateDirectory(currentSavegameFile.variable);
        }
        gameSave_Info.Scene = gameCurrent_Info.Scene;
        gameSave_Info.SceneRespawnIndex = gameCurrent_Info.SceneRespawnIndex;
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream saveFile = File.Create(currentSavegameFile.variable+"/save.binary");
        for (int i = 0; i < objectsToPersist.Count; i++)
        {
            FileStream SOsaveFile = File.Create(currentSavegameFile.variable + string.Format("/{0}_{1}.pso",objectsToPersist[i].name,i));
            var persistantSO = JsonUtility.ToJson(objectsToPersist[i]);
            formatter.Serialize(SOsaveFile, persistantSO);
            SOsaveFile.Close();
        }
        if (playerobj != null)
        {
            playerSave_Info = playerobj.GetComponent<PlayerStats>().stats;
        }
        formatter.Serialize(saveFile, playerSave_Info.health);
        formatter.Serialize(saveFile, gameSave_Info);
        saveFile.Close();
        print("Finished Saving"+ currentSavegameFile.variable);

        

    }
    public void LoadGameState(bool loadgame = true) {
        if (File.Exists(currentSavegameFile.variable + "/save.binary"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream saveFile = File.Open(currentSavegameFile.variable + "/save.binary", FileMode.Open);
            for (int i = 0; i < objectsToPersist.Count; i++)
            {
                if (File.Exists(currentSavegameFile.variable + string.Format("/{0}_{1}.pso", objectsToPersist[i].name, i)))
                {
                    FileStream SOsaveFile = File.Open(currentSavegameFile.variable + string.Format("/{0}_{1}.pso", objectsToPersist[i].name, i), FileMode.Open);
                    JsonUtility.FromJsonOverwrite((string)formatter.Deserialize(SOsaveFile), objectsToPersist[i]);
                    SOsaveFile.Close();
                }

            }
            playerSave_Info.health = (int)formatter.Deserialize(saveFile);
            gameSave_Info = (GameState)formatter.Deserialize(saveFile);
            saveFile.Close();
            if (loadgame)
            {
                StartCoroutine(MoveSceneManager.LoadSceneAsyncWithoutPlayer(gameSave_Info.Scene, gameSave_Info.SceneRespawnIndex));
            }
            print("Finished Loading:" + currentSavegameFile.variable);
        }
        else {
            Debug.Log("No Save Game Found");
        }
    }
    public void SaveSettings() {
        if (!Directory.Exists("Config"))
        {
            Directory.CreateDirectory("Config");
        }
        BinaryFormatter formatter = new BinaryFormatter();
        for (int i = 0; i < configFloats.Count; i++)
        {
            FileStream SOsaveFile = File.Create("Config" + string.Format("/{0}_{1}.pso", configFloats[i].name, i));
            var persistantSO = JsonUtility.ToJson(configFloats[i]);
            formatter.Serialize(SOsaveFile, persistantSO);
            SOsaveFile.Close();
        }
        print("Finished Saving:" + "Config");
    }
    public void LoadSetting() {
        if (Directory.Exists("Config"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            for (int i = 0; i <= objectsToPersist.Count; i++)
            {
                if (File.Exists("Config" + string.Format("/{0}_{1}.pso", configFloats[i].name, i)))
                {
                    FileStream SOsaveFile = File.Open("Config" + string.Format("/{0}_{1}.pso", configFloats[i].name, i), FileMode.Open);
                    JsonUtility.FromJsonOverwrite((string)formatter.Deserialize(SOsaveFile), configFloats[i]);
                    SOsaveFile.Close();
                }

            }
            print("Finished Loading:" + "Config");
        }

    }



    [ContextMenu("DeleteSave")]
    public void DeleteGameState() {
        if (Directory.Exists(currentSavegameFile.variable))
        {
            for (int i = 0; i <= Directory.GetFiles(currentSavegameFile.variable).Length-1; i = 0)
            {
                print("Erased File:" + Directory.GetFiles(currentSavegameFile.variable)[i]);
                File.Delete(Directory.GetFiles(currentSavegameFile.variable)[i]);
            }
            Directory.Delete(currentSavegameFile.variable);
        }

    }
    #endregion
    #region BossRank_UI
    public void StartEncounter() {
        Cursor.visible = false;
        aliveState = true;
        earlyLeave = false;
        StartCoroutine(FightStart());
    }
    public IEnumerator FightStart()
    {
        TextMeshProUGUI fightTimer;
        TextMeshProUGUI fightRank;
        float timeElapsed = 0;
        float timeDisplaySec = 0;
        float timeDisplayMin = 0;
        float displayspeed = 0.2f;
        while (aliveState)
        {
            timeElapsed += 1;
            yield return new WaitForSeconds(1);
            if (earlyLeave) {
                aliveState = false;
                earlyLeave = false;
                yield break;
            }
        }
        if (!aliveState)
        {
            PlayerBehaviour PlayerControl = playerobj.GetComponent<PlayerBehaviour>();
            notPausable = true;
            PlayerControl.enabled = false;
            PlayerControl.StopMoving();
            playerobj.GetComponent<Animator>().Rebind();
            GameObject displayer = Instantiate(rankStats, Vector3.zero, Quaternion.identity);
            fightTimer = GameObject.Find("Time").GetComponent<TextMeshProUGUI>();
            fightRank = GameObject.Find("Rank").GetComponent<TextMeshProUGUI>();
            fightRank.text = "";
            while (timeDisplaySec != timeElapsed) {
                
                timeDisplaySec += 1;
                if (timeDisplaySec == 60) {
                    timeDisplaySec = 0;
                    timeDisplayMin += 1;
                    timeElapsed -= 60;
                }
                fightTimer.text = "Time " +timeDisplayMin.ToString("00") +":"+ timeDisplaySec.ToString("00´");
                yield return new WaitForSeconds(displayspeed);
                displayspeed -= 0.01f;
            }
            PlayerControl.StopMoving();
            string Rank = CircleBehaviour.instance.RankLetters(Mathf.RoundToInt(timeDisplayMin*60+timeDisplaySec));
            yield return new WaitForSeconds(0.4f);
            fightRank.text += Rank[0];
            yield return new WaitForSeconds(0.4f);
            fightRank.text += Rank[1];
            while (true) {
                if (Input.anyKey) {
                    GameObject.Find("Ranking_BG").GetComponent<Animator>().SetTrigger("Fade");
                    yield return new WaitForSeconds(2.3f);
                    PlayerControl.enabled = true;
                    notPausable = false;
                    Destroy(displayer);
                    finishScore.Raise();
                    yield break;
                }
                yield return null;
            }
            
        }
        
    }
    public void endEncounter() {aliveState = false; Cursor.visible = true; }
    #endregion
    public void StartScene()
    {
        StartCoroutine(MoveSceneManager.LoadSceneAsyncWithoutPlayer(Mathf.RoundToInt(scene_SpawnPoint_FirstLevel.x), Mathf.RoundToInt(scene_SpawnPoint_FirstLevel.y)));
    }
    public void MainMenuScene()
    {
        StartCoroutine(MoveSceneManager.LoadSceneAsyncEmpty(0));
    }

}






[System.Serializable]
public class GameState
{
    [Header("Game Parameters")]
    public int Scene;
    public int SceneRespawnIndex;

}

