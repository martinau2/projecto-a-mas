﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResolutionManage : MonoBehaviour
{
    public bool maintainWidth = true;
    public float defaultWidth = 32;
    public float screenDesiredHeight = 1080;
    public float screenDesiredWidth = 1920;
    public int orthopedicDesiredValue;

    // Use this for initialization
    void Start()
    {
        OnChangedRes();
        //defaultWidth = Camera.main.orthographicSize*Camera.main.aspect;
    }

    public void OnChangedRes()
    {
        defaultWidth = orthopedicDesiredValue * (screenDesiredWidth / screenDesiredHeight);
    }

    private void Update()
    {
        if (maintainWidth) {
            Camera.main.orthographicSize = defaultWidth / Camera.main.aspect;
        }
    }

}
