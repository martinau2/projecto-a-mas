﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class CameraShake : MonoBehaviour
{
    public float shakeDuration;
    public float shakeMagnitude;
    public PostProcessProfile profile;
    ChromaticAberration ca;

    private void Awake()
    {
        if (profile != null)
        {
            profile.TryGetSettings(out ca);
            if (ca != null)
                ca.intensity.value = 0;
        }
    }

    public void CameraShaker() {
        StartCoroutine(Shake(shakeDuration, shakeMagnitude));
    }


    public IEnumerator Shake(float duration, float magnitude) {
        Vector3 currentPos = transform.position;
        float elapsed = 0;
        while (elapsed < duration) {
            if (ca != null)
            {
                ca.intensity.value = Random.Range(0.2f, 0.4f);
            }
            float x = magnitude * Random.Range(0, 2);
            float y = magnitude * Random.Range(0, 2);

            transform.position = new Vector3(currentPos.x + x, currentPos.y + y, currentPos.z);

            elapsed += Time.deltaTime;

            yield return null;
        }
        if (ca != null)
        {
            ca.intensity.value = 0;
        }
        transform.position = currentPos;
    }
}
