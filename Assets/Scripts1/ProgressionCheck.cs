﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ProgressionCheck : MonoBehaviour
{

    public ScriptableInt progressionValue;
    public int desiredProgressionValue;
    public UnityEvent OnEqualProgression;
    public UnityEvent OnFurtherProgression;
    public UnityEvent OnUnderProgression;

    public bool diferentProgression;

    public UnityEvent OnDiferentProgression;


    private void Update()
    {
        CheckProgressionEvents();
    }

    public void CheckProgressionEvents()
    {
        if (desiredProgressionValue == progressionValue.variable)
        {
            OnEqualProgression.Invoke();
        }
        else if (desiredProgressionValue > progressionValue.variable && !diferentProgression)
        {
            OnUnderProgression.Invoke();
        }
        else if (desiredProgressionValue < progressionValue.variable && !diferentProgression)
        {
            OnFurtherProgression.Invoke();
        }
        else if (diferentProgression && desiredProgressionValue != progressionValue.variable)
        {
            OnDiferentProgression.Invoke();
        }
    }
}
