﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickInputs : MonoBehaviour
{
    public Canvas AndroidCanvas;
    public ScriptableFloats joystickHorizontal;
    public ScriptableFloats joystickVertical;
    public Joystick Joystick;

    private void Start()
    {

        AndroidCanvas.enabled = Application.platform == RuntimePlatform.Android ? true : false;
    }

    void Update()
    {
        joystickHorizontal.variable = Joystick.Horizontal;
        joystickVertical.variable = Joystick.Vertical;
    }
}
