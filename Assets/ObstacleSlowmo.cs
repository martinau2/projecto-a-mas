﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSlowmo : MonoBehaviour
{
    public Collider2D ActiveCollider;
    public Collider2D SecondActiveCollider;

    public void SwitchCollider() {
        ActiveCollider.enabled = !ActiveCollider.enabled;
        SecondActiveCollider.enabled = !SecondActiveCollider.enabled;
    }
}
