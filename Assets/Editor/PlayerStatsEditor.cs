﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PlayerStats))]
public class PlayerStatsEditor : Editor
{
    SerializedProperty invulnerable;

    static bool showColors;
    static bool showNormalInspector;

    private void OnEnable()
    {
        invulnerable = serializedObject.FindProperty("invulnerability");
    }
    public override void OnInspectorGUI()
    {
        var player = target as PlayerStats;
        showNormalInspector = GUILayout.Toggle(showNormalInspector, "Show Default Inspector");
        GUIStyle StyleofText = new GUIStyle(EditorStyles.label);
        

        StyleofText.normal.textColor = invulnerable.boolValue ? Color.red : Color.white;
        EditorGUILayout.LabelField("Currently Invulnerable =" + invulnerable.boolValue.ToString(),StyleofText);


        if (showNormalInspector)
        {
            DrawPropertiesExcluding(serializedObject, "invulnerability", "material", "playerColor", "hitColor");
        }

        EditorExtendor.SeparatorLine();

        if (GUILayout.Button("Show Player Colors")) {
            if (showColors == true)
            {
                showColors = false;
            }
            else
            {
                showColors = true;
            }
        }
        if (showColors)
        {
            serializedObject.Update();
            DrawPropertiesExcluding(serializedObject, "invulnerability", "material","Sprite", "stats", "inmunityTimer"
                , "hudHP","deathEvent","sp","hit","script", "m_Script");
            serializedObject.ApplyModifiedProperties();
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Test Hit Color"))
            {
                player.material.SetColor("_MainColor", player.hitColor);
            }
            else if (GUILayout.Button("Test Player Color")) {
                player.material.SetColor("_MainColor", player.playerColor);
            }
            GUILayout.EndHorizontal();
        }
    }
}
