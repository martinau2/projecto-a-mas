﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EditorExtendor : Editor {
    public static void SeparatorLine()
    {
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
    }
}

[CustomEditor(typeof(Boss),true)]
public class BossEditor : Editor
{
    SerializedProperty health;
    SerializedProperty dead;
    SerializedProperty player;
    SerializedProperty times;
    SerializedProperty rank;
    SerializedProperty startEvent,endEvent,dmgEvent;

    static bool showEvents;
    static bool showDMGTXTHANDLE;

    private void OnEnable()
    {
        health = serializedObject.FindProperty("bossHealth");
        dead = serializedObject.FindProperty("dead");
        startEvent = serializedObject.FindProperty("startEncounter");
        endEvent = serializedObject.FindProperty("finishEncounter");
        dmgEvent = serializedObject.FindProperty("dmgEvent");
        player = serializedObject.FindProperty("player");
        times = serializedObject.FindProperty("times");
        rank = serializedObject.FindProperty("rank");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        var boss = target as Boss;
        GUIStyle StyleofText = new GUIStyle(EditorStyles.label);


        GUILayout.BeginHorizontal();
        StyleofText.normal.textColor = health.floatValue == 0 ? Color.red : Color.white;
        EditorGUILayout.LabelField("Current Health = " + health.floatValue, StyleofText);
        StyleofText.normal.textColor = dead.boolValue ? Color.red : Color.white;
        EditorGUILayout.LabelField("State = " + (dead.boolValue ? "Dead" : "Alive"), StyleofText);

        GUILayout.EndHorizontal();

        EditorExtendor.SeparatorLine();
        #region Events
        if (GUILayout.Button("Boss Events"))
        {
            if (showEvents == true)
            {
                showEvents = false;
            }
            else
            {
                showEvents = true;
            }
        }
        if (showEvents)
        {
            EditorGUILayout.ObjectField(startEvent);
            EditorGUILayout.ObjectField(endEvent);
            EditorGUILayout.ObjectField(dmgEvent);
        }
        #endregion


        EditorExtendor.SeparatorLine();

        #region Player GO Display
        if (player.objectReferenceValue != null)
        {
            EditorGUILayout.ObjectField(player);
            StyleofText.normal.textColor = Color.white;
            EditorGUILayout.LabelField("Player Found", StyleofText);
        }
        else
        {
            StyleofText.normal.textColor = Color.red;
            EditorGUILayout.LabelField("Player Not Yet Found", StyleofText);
        }
        #endregion

        EditorExtendor.SeparatorLine();


        EditorGUILayout.LabelField("Reward Table:");
        GUILayout.BeginHorizontal();
        EditorGUILayout.PropertyField(rank, new GUIContent("Rank"), true);
        EditorGUILayout.PropertyField(times, new GUIContent("Times"), true);
        GUILayout.EndHorizontal();
        GUI.enabled = false;
        EditorGUILayout.TextArea("Times [0] and [1] correspond to Rank[0]\nTimes [1] and [2] correspond to Rank[1]...");
        GUI.enabled = true;
        EditorExtendor.SeparatorLine();


        DrawPropertiesExcluding(serializedObject, "bossHealth", "dead", "startEncounter", "finishEncounter", "dmgEvent", "player", "rank", "times");
        if (GUILayout.Button("Show Dmg Text Handle"))
        {
            if (showDMGTXTHANDLE == true)
            {
                showDMGTXTHANDLE = false;
            }
            else
            {
                showDMGTXTHANDLE = true;
            }
        }


        serializedObject.ApplyModifiedProperties();
        serializedObject.Update();
    }
    

    private void OnSceneGUI()
    {
        GUIStyle StyleofText = new GUIStyle(EditorStyles.label);

        var boss = target as Boss;

        if (showDMGTXTHANDLE)
        {
            StyleofText.normal.textColor = Color.white;
            Handles.Label(boss.transform.position + new Vector3(0, boss.yOffSet, 0), "DMG TEXT", StyleofText);
            EditorGUI.BeginChangeCheck();
            Vector3 position = Handles.DoPositionHandle(boss.transform.position + new Vector3(0, boss.yOffSet, 0), Quaternion.identity);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "Change Scale Value");
                boss.yOffSet = position.y - boss.transform.position.y;

            }
        }
    }
}
