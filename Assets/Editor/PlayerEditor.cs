﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlayerBehaviour))]
public class PlayerEditor : Editor
{
    SerializedProperty moveInput;
    static float moveInput_ = 0.0f;
    SerializedProperty stats;
    SerializedProperty dead;
    static bool showStats;
    static bool showNormalInspector;
    

    private Editor StatsEditor;

    void OnEnable()
    {
        moveInput = serializedObject.FindProperty("moveInput");
        stats = serializedObject.FindProperty("stats");
        dead = serializedObject.FindProperty("dead");
        StatsEditor = Editor.CreateEditor(stats.objectReferenceValue);
    }

    public override void OnInspectorGUI()
    {
        var player = target as PlayerBehaviour;

        serializedObject.Update();
        SerializedObject statsinstance = new SerializedObject(serializedObject.FindProperty("stats").objectReferenceValue);

        GUIStyle StyleofText = new GUIStyle(EditorStyles.label);
        EditorExtendor.SeparatorLine();
        #region Displays Health and Alive State of the Player
        //Shows Player Current State
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Current Health = "+statsinstance.FindProperty("health").intValue.ToString());
        statsinstance.ApplyModifiedProperties();
        GUILayout.Space(-55);
        EditorGUILayout.LabelField("Current Player State =");
        GUILayout.Space(-55);
        //Shows Player alive state
        StyleofText.normal.textColor = dead.boolValue == false ? Color.white : Color.red;
        EditorGUILayout.LabelField((dead.boolValue == false ? "Alive" : "Dead"), StyleofText);
        GUILayout.EndHorizontal();
        #endregion
        EditorExtendor.SeparatorLine();
        #region Buttons That Decide if displaying Stats or Default Inspector
        GUILayout.BeginHorizontal();

        //Show Stats Bool
        showStats = GUILayout.Toggle(showStats, "Show Stats/");
        //Shows Stats Scriptable Object Inspector
        showNormalInspector = GUILayout.Toggle(showNormalInspector, "Show Default Inspector");
        GUILayout.EndHorizontal();
        #endregion


        if (showStats)
        {
            EditorGUILayout.LabelField("Player Stats ScriptableObject");
            EditorGUILayout.ObjectField(stats);
            StatsEditor.OnInspectorGUI();

        }
        //Shoes Normal Inspector
        if (showNormalInspector)
        {
            DrawPropertiesExcluding(serializedObject, "moveInput", "stats","dead");
        }
        EditorExtendor.SeparatorLine();
        #region Desplays Moving Data, Grounded Data, And Current Animation Data
        GUI.color = Color.white;
        //Shows if player is moving
        if (Mathf.Abs(moveInput.floatValue) > 0)
        {
            GUILayout.Space(10);
            GUILayout.BeginHorizontal();
            GroundCheck(player, StyleofText);
            GUILayout.Space(-25);
            StyleofText.normal.textColor = Color.yellow;
            EditorGUILayout.LabelField("Current State = Moving ", StyleofText, GUILayout.Width(200));
            GUILayout.EndHorizontal();
            moveInput_ = EditorGUILayout.Slider("MoveInput:", moveInput.floatValue, -1, 1, GUILayout.Width(300));
        }
        else
        {
            GUILayout.Space(10);
            GUILayout.BeginHorizontal();
            GroundCheck(player, StyleofText);
            GUILayout.Space(-25);
            StyleofText.normal.textColor = Color.white;
            EditorGUILayout.LabelField("Current State ="+" Idle",StyleofText, GUILayout.Width(200));
            GUILayout.EndHorizontal();
            moveInput_ = EditorGUILayout.Slider("MoveInput:",moveInput.floatValue, -1, 1, GUILayout.Width(300));
        }
        if (player.animator != null)
        {
            GUILayout.Label(player.animator.GetCurrentAnimatorClipInfo(0)[0].clip.name);
        }
        #endregion
        EditorExtendor.SeparatorLine();
        serializedObject.ApplyModifiedProperties();
        serializedObject.Update();
    }

    //Checks if player Grounded
    private static void GroundCheck(PlayerBehaviour player, GUIStyle StyleofText)
    {
        if (player.isGrounded)
        {
            StyleofText.normal.textColor = Color.white;
            EditorGUILayout.LabelField("Current Air State = Grounded ", StyleofText, GUILayout.Width(200));
        }
        else
        {
            StyleofText.normal.textColor = Color.yellow;
            EditorGUILayout.LabelField("Current Air State = Air ", StyleofText, GUILayout.Width(200));
        }
    }

    private void OnSceneGUI()
    {
        GUIStyle StyleofText = new GUIStyle(EditorStyles.label);
        var player = target as PlayerBehaviour;
        var feet = player.feet;
        StyleofText.normal.textColor = Color.white;
        Handles.Label(feet.position-new Vector3(0,0.2f,0), "Feet Pos",StyleofText);
        
        float size = HandleUtility.GetHandleSize(feet.position) *1;
        float snap = 0.5f;
        Handles.color = player.isGrounded ? Color.green:Color.red;


        EditorGUI.BeginChangeCheck();
        float scale = Handles.ScaleSlider(feet.localScale.x, feet.position, feet.right, Quaternion.identity, size, snap);
        Handles.SphereHandleCap(0, feet.position, Quaternion.identity, player.checkRadius*2,EventType.Repaint);
        if (EditorGUI.EndChangeCheck()) {
            Undo.RecordObject(target, "Change Scale Value");
            feet.localScale = new Vector3(scale,feet.localScale.y,feet.localScale.z);
            player.checkRadius = feet.localScale.x;
            
        }
    }


}


[CustomEditor(typeof(PlayerState))]
public class PlayerStateEditor : Editor {


    SerializedProperty description;
    static bool showDes = false;
    static bool showCurr = false;
    static bool showInit = false;
    private void OnEnable()
    {
        description = serializedObject.FindProperty("Description");

    }

    public override void OnInspectorGUI() {
        serializedObject.Update();
        PlayerState myTarget = (PlayerState)target;
        showDes = GUILayout.Toggle(showDes, "Show Description");

        #region Displays Description
        if (showDes)
        {
            GUI.enabled = false;
            GUILayout.TextArea(description.stringValue);
            GUI.enabled = true;
        }
        #endregion
        EditorExtendor.SeparatorLine();
        #region Buttons that deplays the 2 types of stats
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Show Current Stats"))
        {
            if (showCurr == true)
            {
                showCurr = false;
            }
            else
            {
                showCurr = true;
                showInit = false;
            }
        }
        else if (GUILayout.Button("Show Initial Stats")) {
            if (showInit == true)
            {
                showInit = false;
            }
            else
            {
                showCurr = false;
                showInit = true;
            }
        }
        GUILayout.EndHorizontal();
        #endregion

        #region Displays the 2 types of stats
        if (showCurr)
        {
            GUI.enabled = false;
            GUI.color = Color.yellow;
            DrawPropertiesExcluding(serializedObject, "initial_Bullet", "initial_PowerUps", "initial_JumpForce",
                "initial_Speed", "initial_RechargeRate", "initial_Slowmo", "initial_MaxSlowmo", "initial_Health", "initial_MaxHealth","Description");
            
        }
        else if (showInit) {
            GUI.enabled = true;
            GUI.color = Color.cyan;
            DrawPropertiesExcluding(serializedObject, "bullet", "powerUps", "jumpForce",
                "speed", "rechargeRate", "slowmo", "maxSlowmo", "health", "maxHealth", "Description");
            
        }
        if (showInit == true && showCurr == true) {
            showInit = false;
            showCurr = false;
        }
        #endregion

        GUI.enabled = true;
        GUI.color = Color.white;
        if (GUILayout.Button("Restore to Init Values"))
        {
            myTarget.restore();
        }
        serializedObject.ApplyModifiedProperties();
        serializedObject.Update();
    }
}




