﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossIdleState : StateMachineBehaviour
{
    [SerializeField]
    BossBehaviour behaviour;

    public float timer;
    public float minTime;
    public float maxTime;
    public float nextPattern = 10;
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        behaviour = animator.GetComponent<BossBehaviour>();
        timer = Random.Range(minTime, maxTime);
        nextPattern = 10;
        
        
    }


    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.transform.rotation = Quaternion.Lerp(animator.transform.rotation, Quaternion.Euler(new Vector3(0, 0, 0)), 0.1f);
        if (timer <= 0) {
            if (nextPattern == 10)
            {
                nextPattern = Random.Range(0, 2);
            }
            if (nextPattern == 0)
            {
                animator.SetTrigger("bulletAttck");

            }
            else if (nextPattern == 1) {
                animator.SetTrigger("rayAttck");
            }

        }
        else{
            timer -= Time.deltaTime;



        }

    }

    
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger("bulletAttck");
        animator.ResetTrigger("rayAttck");
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
