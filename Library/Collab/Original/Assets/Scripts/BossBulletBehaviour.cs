﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBulletBehaviour : MonoBehaviour
{
    Rigidbody2D rb;
    public float speed;
    public LayerMask bounce;
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
        RaycastHit2D collide = Physics2D.Raycast(transform.position, transform.position + transform.right, 1.7f, bounce);
        Debug.DrawLine(transform.position, transform.position+transform.right * 1.7f, Color.red);
        if (collide)
        {
            if (collide.collider.tag == "Ground")
            {
                Quaternion angle = Quaternion.FromToRotation(gameObject.transform.right, new Vector3(collide.normal.x, collide.normal.y, 0));
                transform.rotation = transform.rotation * angle;
            }
            else if (collide.collider.tag == "Player")
            {
                BossBehaviour.instance.dmgEvent.Invoke(1,collide.point);
                Destroy(gameObject);
            }
        }
    }
    private void FixedUpdate()
    {
        rb.velocity = transform.right*speed;
        
    }
    
}
