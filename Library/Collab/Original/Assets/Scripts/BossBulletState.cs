﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBulletState : StateMachineBehaviour
{
    BossBehaviour behaviour;
    public float setTimer = 3;
    float timer;
    float shootTimer;
    public float setShootTimer;
    public float shotSpeed;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        behaviour = animator.GetComponent<BossBehaviour>();
        timer = setTimer;
        shootTimer = setShootTimer;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        behaviour.rotateToPlayer();
        if (shootTimer <= 0)
        {
            behaviour.ShootBullet();
            shootTimer = setShootTimer;
        }
        else {
            shootTimer -= Time.deltaTime;
        }
        
        if (timer <= 0)
        {
            animator.SetTrigger("idle1");

        }
        else {
            timer -= Time.deltaTime;
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger("idle1");
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
